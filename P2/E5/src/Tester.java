import java.util.*;

public class Tester{

	public static void main(String[] args){
		int npiezas = 25;

		Tienda tienda = new Tienda();

		Articulo carrito1 = new Carrito(npiezas, "1", "marca", "modelo", "color");
		Articulo carrito2 = new Carrito(npiezas, "2", "marca", "modelo", "color");
		Articulo cuna1 = new Cuna(npiezas, "3");
		Articulo cuna2 = new Cuna(24, "4"); //tiene 24 piezas, por lo que no va a pasar el primer control
		Articulo silla1 = new Silla(npiezas, "5", "marca", "modelo", "color", 1, 3, true);
		Articulo silla2 = new Silla(npiezas, "6", "marca", "modelo", "color", 1, 3, true);


/*
1. Simule la recepción de distintos artículos
2. Simule el proceso de verificación de todos los artículos recepcionados
3. Realice el envío a fabricante de algunos de los artículos que pueden ser reparados
4. Muestre un inventario de todos los productos almacenados en el almacén
5. Seleccione tres sillitas en perfecto estado para simular una venta a un cliente (es
suficiente con mostrar en pantalla la información de las tres primeras sillitas
del inventario en el estado requerido)
*/
		tienda.recibirArticulo(carrito1);
		tienda.recibirArticulo(carrito2);
		tienda.recibirArticulo(cuna1);
		tienda.recibirArticulo(cuna2);
		tienda.recibirArticulo(silla1);
		tienda.recibirArticulo(silla2);

		System.out.println("Stock despues de añadir todos los artículos en la tienda: ");
		tienda.printStock();

		System.out.println("===================================================================================");

		tienda.control1();//La cuna con codigo 4 tiene que salir del stock, ya que no tiene suficientes piezas
		System.out.println("Stock despues de hacer el primer control: ");
		tienda.printStock();

		System.out.println("===================================================================================");

		/*
		carrito2 (codigo 2) tiene mal la pintura, por lo que tendra que ser enviado a Outlet
		silla1 (codigo 5) tiene mal el acolchado, por lo que tiene que ser enviado a reparando
		*/
		tienda.control2();
		System.out.println("Despues de hacer el segundo control: ");
		System.out.println("Stock: ");
		tienda.printStock();
		System.out.println("Outlet: ");
		tienda.printOutlet();
		System.out.println("Artículos devueltos para reparar: ");
		tienda.printReparando();

	}
}
