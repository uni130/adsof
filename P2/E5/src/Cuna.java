/**
*Define el objeto Cuna
*/

public class Cuna extends Articulo{
  private int npiezas = 25; //El número de piezas que tienen que tener las cunas.

  public Cuna(int piezas, String codigo){
    this.piezas = piezas;
    this.codigo = codigo;
  }

  public String toString(){
    return "[Cuna] Código: " + codigo + " Número de piezas: " + piezas;
  }

  public boolean compruebaPiezas(){
    return (this.piezas == npiezas);
  }

  public boolean plegar(){
    return true;
  }

  public boolean compruebaTapiceria(){
    return true;
  }
  public boolean compruebaPintura(){
    return true;
  }

  public void setComentario(String comentario){}
  public boolean compruebaEnganche(){
    return true;
  }
  public boolean compruebaAnclajes(){
    return true;
  }
  public boolean compruebaAcolchado(){
    return true;
  }
}
