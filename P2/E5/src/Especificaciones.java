/**
*Define la clase abstracta articulo
*/
public abstract class Especificaciones extends Articulo{
  protected String marca;
  protected String modelo;
  protected String color;
  protected String comentario;

  public abstract boolean compruebaAcolchado();
  public abstract boolean compruebaTapiceria();
  public abstract boolean compruebaAnclajes();
  public abstract boolean compruebaPintura();
  public abstract void setComentario(String comentario);
}
