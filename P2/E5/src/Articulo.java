
/**
*Esta clase define la clase abstracta Articulo
*@author Miguel Díaz y Miguel Ángel Luque
*/

public abstract class Articulo{
  protected int piezas;
  protected String codigo;

  public abstract boolean compruebaPiezas();

  public abstract boolean compruebaAcolchado();
  public abstract boolean compruebaTapiceria();
  public abstract boolean compruebaAnclajes();
  public abstract boolean compruebaPintura();
  public abstract boolean compruebaEnganche();
  public abstract boolean plegar();
  public abstract void setComentario(String comentario);
}
