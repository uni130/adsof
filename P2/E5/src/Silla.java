/**
*Esta clase define el objeto silla
*/

public class Silla extends Especificaciones{

  private int pesoMin;
  private int pesoMax;
  private boolean isofix;

  public Silla(int piezas, String codigo, String marca, String modelo, String color, int pesoMin, int pesoMax, boolean isofix){
    this.piezas = piezas;
    this.codigo = codigo;
    this.marca = marca;
    this.modelo = modelo;
    this.color = color;
    this.pesoMin = pesoMin;
    this.pesoMax = pesoMax;
    this.isofix = isofix;
  }

  public String toString(){
    return "[Silla] Código: " + codigo + " Número de piezas: " + piezas +
           " Marca: " + marca + " Modelo: " + modelo + " Color: " + color +
           " Peso mínimo: " + pesoMin + " Peso máximo: " + pesoMin + " Isofix: " + isofix;
  }

  public boolean compruebaPiezas(){
    return true;
  }

  public boolean compruebaAcolchado(){
    if(codigo == "5"){
      return false;
    }
    return true;
  }

  public boolean compruebaTapiceria(){
    return true;
  }

  public boolean compruebaAnclajes(){
    return true;
  }

  public boolean compruebaPintura(){
    return true;
  }

  public void setComentario(String comentario){
    this.comentario = comentario;
  }

  public boolean plegar(){
    return true;
  }
  public boolean compruebaEnganche(){
    return true;
  }

}
