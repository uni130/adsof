import java.util.*;


/**
*Esta clase define la clase tienda
*@author Miguel Díaz y Miguel Ángel Luque
*/

public class Tienda{
  private List<Articulo> stock = new ArrayList<>();
  private List<Articulo> outlet = new ArrayList<>();
  private List<Articulo> reparando = new ArrayList<>();

  public void recibirArticulo(Articulo articulo){
    stock.add(articulo);
  }

  /**
  *Hace el primer control de calidad
  */

  public void control1(){
    List<Articulo> aux = new ArrayList<>();/*lista auxiliar de los elementos que se van a eliminar de stock*/

    for(Articulo a: stock){
       if(!(a.compruebaPiezas())){
         aux.add(a);
       }
    }

    for(Articulo a: aux){
      stock.remove(a);
    }
  }



  /**
  *Hace el segundo control de calidad
  */

  public void control2(){
    List<Articulo> aux = new ArrayList<>();/*lista auxiliar de los elementos que se van a eliminar de stock*/

    for(Articulo a: stock){
      if(!(a.plegar())){
        aux.add(a);
      }
    }



    for(Articulo a: stock){
      if(!(a.compruebaEnganche()) || !(a.compruebaAnclajes()) || !(a.compruebaAcolchado()) ){
        reparando.add(a);
        aux.add(a);
      }
    }

    for(Articulo a: stock){
      if(!(a.compruebaTapiceria()) || !(a.compruebaPintura())){
        outlet.add(a);
        aux.add(a);
      }
    }

    for(Articulo a: aux){
      stock.remove(a);
    }

  }

  public void printStock(){
    for(Articulo a: stock){
      System.out.println(a);
    }
  }

  public void printOutlet(){
    for(Articulo a: outlet){
      System.out.println(a);
    }
  }

  public void printReparando(){
    for(Articulo a: reparando){
      System.out.println(a);
    }
  }

}
