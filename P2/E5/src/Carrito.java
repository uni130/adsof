
/**
*Esta clase define el objeto Carrito
*@author Miguel Díaz y Miguel Ángel Luque
*/


public class Carrito extends Especificaciones{

  public Carrito(int piezas, String codigo, String marca, String modelo, String color){
    this.piezas = piezas;
    this.codigo = codigo;
    this.marca = marca;
    this.modelo = modelo;
    this.color = color;
  }

  public  String toString(){
    return "[Carrito] Código: " + codigo + " Número de piezas: " + piezas +
            " Marca: " + marca + " Modelo: " + modelo + " Color: " + color;
  }

  public boolean compruebaPiezas(){
    return true;
  }

  public boolean compruebaEnganche(){
    return true;
  }

  public boolean compruebaAcolchado(){
    return true;
  }

  public boolean compruebaTapiceria(){
    return true;
  }

  public boolean compruebaAnclajes(){
    return true;
  }

  public boolean compruebaPintura(){
    if(codigo == "2"){
      return false;
    };
    return true;
  }

  public boolean plegar(){
    return true;
  }

  /**
  *@param el comentario que se quiere poner cuando se manda a reparar
  */
  public void setComentario(String comentario){
    this.comentario = comentario;
  }
}
