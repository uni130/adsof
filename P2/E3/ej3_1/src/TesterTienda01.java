/**
*
*este fichero sirve para verificar la funcionalidad de las clases Pelicula,
*Libro, Disco y Producto
*
*@author Miguel Angel Luque y Miguel Diaz
*
*/

public class TesterTienda01 {
   public static void main(String[] args) {
      Libro l = new Libro(1, "La historia interminable", "Ende, Michael", "Ed. Alfaguara");
      Disco d = new Disco(12, "Evanescence", "Fallen", 2003);
      Pelicula p = new Pelicula(34, "Cadena Perpetua", "Drama", "Frank Darabont");

      System.out.println(l);
      System.out.println(d);
      System.out.println(p);
   }
}
