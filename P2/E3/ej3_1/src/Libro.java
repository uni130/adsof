/**
*
*esta clase contiene las funciones y variables para interactuar con un libro
*
*@author Miguel Angel Luque y Miguel Diaz
*
*/

public class Libro extends Producto{
  private String autor;
  private String editorial;
  public Libro(long idd,String tit,String aut,String ed){
    id = idd;
    titulo = tit;
    autor = aut;
    editorial = ed;
  }

  /**
  *
  *@return autor del libro
  */
  public String getAutor(){
    return this.autor;
  }

  /**
  *@param aut sustituye el autor del libro preestablecido
  *
  */
  public void setAutor(String aut){
    this.autor = aut;
  }

  /**
  *
  *@return editorial que publico el libro
  */
  public String getEditorial(){
    return this.editorial;
  }

  /**
  *@param edit sustituye el editorial del libro preestablecido
  *
  */
  public void setEditorial(String edit){
    this.editorial = edit;
  }

  /**
  *
  *@return devuelve la cadena a imprimir que da todos los datos del libro
  */
  public String toString(){
    String idd;
    if(id<10){
      idd = "0" + id;
    }else{
      idd = "" + id;
    }
    return "["+ idd +"] " + "LIBRO: " + titulo + ". " + autor + ". " + editorial;
  }
}
