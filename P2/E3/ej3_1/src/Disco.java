/**
*
*esta clase contiene las funciones y variables para interactuar con un disco
*
*@author Miguel Angel Luque y Miguel Diaz
*
*/

public class Disco extends Producto{
  private String interprete;
  private int anio;
  public Disco(long idd,String tit,String inte,int an){
    id = idd;
    titulo = tit;
    interprete = inte;
    anio = an;
  }

  /**
  *
  *@return interprete del disco
  */
  public String getInterprete(){
    return this.interprete;
  }

  /**
  *@param inte sustituye el interprete preestablecido del disco
  *
  */
  public void setInterprete(String inte){
    this.interprete = inte;
  }

  /**
  *
  *@return año de salida del disco disco
  */
  public int getAnio(){
    return this.anio;
  }

  /**
  *
  *@param an sustituye el año de salida preestablecido del disco
  */
  public void setAnio(int an){
    this.anio = an;
  }

  /**
  *
  *@return devuelve la cadena a imprimir que da todos los datos del disco
  */
  public String toString(){
    String idd;
    if(id<10){
      idd = "0" + id;
    }else{
      idd = "" + id;
    }
    return "["+ idd +"] " + "DISCO: " + titulo + " - " + interprete + " (" + anio + ")";
  }
}
