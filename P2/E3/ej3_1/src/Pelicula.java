/**
*
*esta clase contiene las funciones y variables para interactuar con una pelicula
*
*@author Miguel Angel Luque y Miguel Diaz
*
*/

public class Pelicula extends Producto{
  private String genero;
  private String director;
  public Pelicula(long idd,String tit,String gen,String dir){
    id = idd;
    titulo = tit;
    genero = gen;
    director = dir;
  }

  /**
  *
  *@return genero de la pelicula
  */
  public String getGenero(){
    return this.genero;
  }

  /**
  *@param gen introduce el nuevo genero de la pelicula
  *
  */
  public void setGenero(String gen){
    this.genero = gen;
  }

  /**
  *
  *@return director de la pelicula
  */
  public String getDirector(){
    return this.director;
  }

  /**
  *@param dir introduce el nuevo director de la pelicula
  *
  */
  public void setDirector(String dir){
    this.director = dir;
  }

  /**
  *
  *@return devuelve la cadena a imprimir que da todos los datos de la película
  */
  public String toString(){
    String idd;
    if(id<10){
      idd = "0" + id;
    }else{
      idd = "" + id;
    }
    return "["+ idd +"] " + "PELICULA: " + titulo + " (" + genero + "). DIR: " + director;
  }
}
