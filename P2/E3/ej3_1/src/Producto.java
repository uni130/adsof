/**
*
*esta clase contiene las funciones y variables comunes a libros, discos y peliculas
*
*@author Miguel Angel Luque y Miguel Diaz
*
*/

public abstract class Producto{
  protected long id;
  protected String titulo;

  /**
  *
  *@return id del producto
  */
  public long getId(){
    return this.id;
  }

  /**
  *@param val sustituye el id del producto preestablecido
  *
  */
  public void setId(long val){
    this.id = val;
  }

  /**
  *
  *@return titulo del producto
  */
  public String getTitulo(){
    return this.titulo;
  }

  /**
  *@param cad sustituye el titulo del producto preestablecido
  *
  */
  public void setTitulo(String cad){
    this.titulo = cad;
  }
}
