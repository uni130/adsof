import java.util.*;

/**
*Esta clase define el objeto Rectángulo
*@author Miguel Díaz
*/

public class Rectangulo  extends Figura {
  private double base;
  private double altura;

  public Rectangulo(double b, double a) {
    base = b;
    altura = a;
  }

  /**
  *@return double la base del rectángulo
  */

  public double getBase() {
    return base;
  }

  /**
  *@return double la altura del rectángulo
  */

  public double getAltura() {
    return altura;
  }

  /**
  *@return double el perímetro del rectángulo
  */

  public double getPerimetro() {
    return 2 * (base + altura);
  }

  /**
  *@return double el área del rectángulo
  */

  public double getArea() {
    return base * altura;
  }

  /**
  *@return boolean true si es un cuadrado y false si no lo es
  */

  public boolean isCuadrado() {
    if(base == altura) {
      return true;
    } else {
      return false;
    }
  }

  public String toString() {
    return "Rectángulo [area=" + getArea() + ", perimetro=" + getPerimetro() + "]";
  }


}
