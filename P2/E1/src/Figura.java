import java.util.*;

public abstract class Figura {

  /**
  *@return double el área de la figura
  */

  public abstract double getArea();

  /**
  *@return double el perímetro de la figura
  */

  public abstract double getPerimetro();

  /**
  *@return boolean true si la figura es mayor que la figura en el argumento
  *@param Figura la figura con la que se quiere comparar el tamaño
  */

  public boolean esMayor(Figura a) {
    return (this.getArea() > a.getArea());
  }
}
