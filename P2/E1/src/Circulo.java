import java.util.*;

/**
*Esta clase define el objeto Círculo
*@author Miguel Ángel Luque
*/


public class Circulo extends Figura {
    private double radio;
    public Circulo(double r){
      radio = r;
    }

    /**
    *@return double el radio del Círculo
    */

    public double getRadio(){
      return(radio);
    }

    /**
    *@return double el perímetro del círculo
    */

    public double getPerimetro(){
      double res = radio * 2 * 3.14159265;
      return(res);
    }

    /**
    *@return double el área del círculo
    */

    public double getArea(){
      double res = radio * radio * 3.14159265;
      return(res);
    }
    public String toString(){
      return "Circulo [area=" + getArea() + ", perimetro=" + getPerimetro() + "]";
    }
  }
