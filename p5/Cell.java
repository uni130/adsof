/**
*Este archivo define la clase Cell
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;

public class Cell<G> extends MatrixElement<List<G>> implements IMatrixElement<List<G>>{
	private Matrix<List<G>> matriz;
	public Cell(int i, int j, Matrix<List<G>> matriz) {
		super(i, j);
		this.matriz = matriz;
		this.element = new ArrayList<G>();
	}	
	
	/**
	*@return lista de los elementos en la cell
	*/
	public List<G> getAgentes(){
		return this.element;
	}
	
	/**
	*@param a elemento a aniadir a la cell
	*/
	public void addAgente(G a) {
		element.add(a);
	}
	
	/**
	*@param a elemento a quitar de la cell
	*/
	public void quitAgente(G a) {
		if(this.element != null) {
			element.remove(a);
		}
		if(this.element != null) {
		}
	}
	
	/**
	*@return string con los datos de la celda
	*/
	public String toString() {
		if(this.element == null) {
			return "0";
		}
		return(""+this.element.size());
	}
	
	/**
	*@return lista de las celdas vecinas
	*/
	public List<Cell<G>> neighbours(){
		List<IMatrixElement<List<G>>> listt;
		List<Cell<G>> end = new ArrayList<Cell<G>>();
		Cell<G> element;
		try {
			listt = matriz.getNeighboursAt(this.getI(), this.getJ());
		}catch(IllegalPositionException i) {
			listt = null;
		}
		for(Object e: listt) {
			element = (Cell<G>)e;
			end.add(element);
		}
		return end;
	}
	
	/**
	*@return lista de los elementos en la cell
	*/
	public List<G> agents(){
		return this.element;
	}
}