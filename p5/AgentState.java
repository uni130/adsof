/**
*Este archivo define la clase AgentState
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.Iterator;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class AgentState extends Agent implements IAgentState{
	 private IAgentWithState owner;
	 private List<String> targetjmp;
	 private List<Predicate<IAgent>> triggerjmp;
	 public AgentState(String name) {
		 super(name);
		 targetjmp = new ArrayList<String>();
		 triggerjmp = new ArrayList<Predicate<IAgent>>();
	 }
	 /**
	 *
	 *@return nombre del agente
	 */
	 public String name() {
		 return this.nombre();
	 }
	 /**
	 *@param target estado al que ir si se cumple la condicion
	 *@param trigger condicion
	 */
	 public void toState(String target, Predicate<IAgent> trigger) {
		 targetjmp.add(target);
		 triggerjmp.add(trigger);
	 }
	 /**
	 *
	 *@return estado despues de las posibles modificaciones
	 */
	 public IAgentState changeState() {
		 Iterator<String> iter1 = targetjmp.iterator();
		 Iterator<Predicate<IAgent>> iter2 = triggerjmp.iterator();
		 while(iter1.hasNext()) {
			 String target = iter1.next();
			 Predicate<IAgent> trigger = iter2.next();
			 if(trigger.test(this.owner)) {
				 return owner.setState(target);
			 }
		 }
		 return this;
	 }
	 
	 /**
	 *@param trigger condicion necesaria para la ejecucion
	 *@param behaviour consecuencia de la condicion
	 *@return agente que realiza la accion y sobre el que se comprueba la condicion
	 */
	 @Override
	 public IAgentWithState addBehaviour(Predicate<IAgent> trigger, Function<IAgent, Boolean> behaviour) {
		 this.tacciones.add(behaviour);
		 this.trigg.add(trigger);
		 return this.owner;
	 }
	 
	 /**
	 *@param behaviour movimiento a ejecutar
	 *@return agente que realiza la accion
	 */
	 @Override
	 public IAgentWithState addBehaviour(Function<IAgent, Boolean> behaviour) {
		 this.acciones.add(behaviour);
		 return this.owner;
	 }
	 
	 /**
	 *@param aws owner of the state
	 *
	 */
	 public void setOwner(IAgentWithState aws) {
		 this.owner = aws;
	 }

	 /**
	 *
	 *@return copy of the agent
	 */
	 @Override
	 public IAgent copy() {
		 AgentState aux;
		 aux = new AgentState(this.nombre());
		 aux.acciones.addAll(this.acciones);
		 aux.tacciones.addAll(this.tacciones);
		 aux.trigg.addAll(this.trigg);
		 aux.triggerjmp.addAll(this.triggerjmp);
		 aux.targetjmp.addAll(this.targetjmp);
		 return (IAgent)aux;
	 }
	 @Override
		public void exec() {
			Boolean continues;
			Iterator<Function<IAgent, Boolean>> iter = acciones.iterator();
			Iterator<Function<IAgent, Boolean>> iter2 = tacciones.iterator();
			Iterator<Predicate<IAgent>> iter3 = trigg.iterator();
			while (iter.hasNext()) {
				Function<IAgent, Boolean> accion = iter.next();
				continues = accion.apply(this.owner);
				if(!continues) {
					iter.remove();
				}
			}while (iter2.hasNext()) {
				Function<IAgent, Boolean> accion2 = iter2.next();
				Predicate<IAgent> trigger = iter3.next();
				if(trigger.test(this.owner)) {
					continues = accion2.apply(this.owner);
				}else {
					continues = true;
				}
				if(!continues) {
					iter.remove();
				}
			}
		}
}
