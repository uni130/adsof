/**
*Este archivo define la clase BasicAgent
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;

public class BasicAgent implements IBasicAgent{
	private String nombre;
	protected Cell cell = null;
	
	public BasicAgent(String n) {
		nombre = n;
	}
	
	/**
	*@param c cell en la que colocarse
	*/
	public void setCell(Cell c) {
		if(c != null) {
			if(this.cell()!=null) {
				this.cell().quitAgente(this);
			}
			c.addAgente(this);
			cell = c;
		}
	}
	
	/**
	*@return nombre del agente
	*/
	public String nombre() {
		return nombre;
	}

	/**
	*@return cell en la que se sit�a el agente
	*/
	@Override
	public Cell cell() {
		return cell;
	}

	/**
	*@return copia del agente
	*/
	@Override
	public IBasicAgent copy() {
		BasicAgent aux;
		aux = new BasicAgent(nombre);
		aux.setCell(cell);
		return aux;
	}
}
