/**
*Este archivo define la interfaz IAgentState
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.function.*;

public interface IAgentState{
 String name();
 void toState(String target, Predicate<IAgent> trigger);
 IAgentState changeState();
 IAgentWithState addBehaviour(Predicate<IAgent> trigger, Function<IAgent, Boolean> behaviour);
 IAgentWithState addBehaviour(Function<IAgent, Boolean> behaviour);
 void exec();
 void setOwner(IAgentWithState aws);
 IAgent copy();
}