
public interface IFunc<One, Two, Three> {
	public Three apply(One one, Two two);
}


