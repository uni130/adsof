/**
*Este archivo define la interfaz IMatrix
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;

public interface IMatrix<T> {
	int getCols();
	int getRows();
	boolean isLegalPosition(int i, int j);
	void addElement(IMatrixElement<T> element) throws IllegalPositionException;
	IMatrixElement<T> getElementAt(int i, int j) throws IllegalPositionException;
	List<IMatrixElement<T>> getNeighboursAt(int i, int j) throws IllegalPositionException;
	List<IMatrixElement<T>> asList();
	List<IMatrixElement<T>> asListSortedBy(Comparator<IMatrixElement<T>> c);
	boolean equals(Object obj);
}
