/**
*Este archivo define la clase ComparadorFilas
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.Comparator;

public class ComparadorFilas<T>  implements Comparator<IMatrixElement<T>>{

	/**
	*@param o1 primer elemento
	*@param o2 segundo elemento
	*@return entero indicando mayoria minoria o igualdad
	*/
	@Override
	public int compare(IMatrixElement<T> o1, IMatrixElement<T> o2) {
		if(o1.getJ() == o2.getJ()) {
			return 0;
		}else {
			return o1.getJ() - o2.getJ();
		}
	}

}