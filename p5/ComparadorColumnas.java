/**
*Este archivo define la clase ComparadorColumnas
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.Comparator;

public class ComparadorColumnas<T>  implements Comparator<IMatrixElement<T>>{

	/**
	*@param o1 primer elemento
	*@param o2 segundo elemento
	*@return entero indicando mayoria minoria o igualdad
	*/
	@Override
	public int compare(IMatrixElement<T> o1, IMatrixElement<T> o2) {
		if(o1.getI() == o2.getI()) {
			return 0;
		}else {
			return o1.getI() - o2.getI();
		}
	}

}
