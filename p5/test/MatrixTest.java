/**
*Este archivo define la clase MatrixTest
*@author Miguel Angel Luque y Miguel Diaz
*/
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 */

public class MatrixTest {

	private Matrix<Integer> matrixint;
	private Matrix<String> matrixstring;
	
	@Before
	public void setUp() throws Exception {
		matrixint = new Matrix<Integer>(6,7);
		matrixstring = new Matrix<String>(4,8);
	}

	/**
	 * Test method for {@link Matrix#getCols()}.
	 */
	@Test
	public void testGetCols() {
		assertEquals(6,matrixint.getCols());
		assertEquals(4,matrixstring.getCols());
	}

	/**
	 * Test method for {@link Matrix#getRows()}.
	 */
	@Test
	public void testGetRows() {
		assertEquals(7,matrixint.getRows());
		assertEquals(8,matrixstring.getRows());
	}

	/**
	 * Test method for {@link Matrix#addElement(IMatrixElement)}.
	 * @throws IllegalPositionException 
	 */
	@Test
	public void testAddElement() throws IllegalPositionException {
		MatrixElement<Integer> e1 = new MatrixElement<Integer>(3, 2, 3);
		MatrixElement<String> e2 = new MatrixElement<String>(3, 2, "a");
		matrixint.addElement(e1);
		System.out.println(matrixint);
		matrixstring.addElement(e2);
		System.out.println(matrixstring);
	}

	/**
	 * Test method for {@link Matrix#isLegalPosition(int, int)}.
	 */
	@Test
	public void testIsLegalPosition() {
		assertFalse(matrixint.isLegalPosition(6, 3));
	}

	/**
	 * Test method for {@link Matrix#getElementAt(int, int)}.
	 * @throws IllegalPositionException 
	 */
	@Test
	public void testGetElementAt() throws IllegalPositionException {
		MatrixElement<Integer> e1 = new MatrixElement<Integer>(3, 2, 3);
		matrixint.addElement(e1);
		assertTrue(matrixint.getElementAt(3, 2).equals(new MatrixElement<Integer>(3, 2, 3)));
	}

	/**
	 * Test method for {@link Matrix#asList()}.
	 * @throws IllegalPositionException 
	 */
	@Test
	public void testAsList() throws IllegalPositionException {
		MatrixElement<Integer> e1 = new MatrixElement<Integer>(1, 2, 3);
		MatrixElement<Integer> e2 = new MatrixElement<Integer>(2, 2, 4);
		MatrixElement<Integer> e3 = new MatrixElement<Integer>(3, 2, 5);
		matrixint.addElement(e1);
		matrixint.addElement(e2);
		matrixint.addElement(e3);
		System.out.println("testAsList");
		for(Object e: matrixint.asList()) {
			System.out.println(e);
		}
	}

	/**
	 * Test method for {@link Matrix#getNeighboursAt(int, int)}.
	 * @throws IllegalPositionException 
	 */
	@Test
	public void testGetNeighboursAt() throws IllegalPositionException {
		MatrixElement<String> e1 = new MatrixElement<String>(1, 0, "a");
		MatrixElement<String> e2 = new MatrixElement<String>(0, 1, "b");
		MatrixElement<String> e3 = new MatrixElement<String>(2, 1, "c");
		matrixstring.addElement(e1);
		matrixstring.addElement(e2);
		matrixstring.addElement(e3);
		System.out.println("testGetNeighboursAt");
		for(Object e: matrixstring.getNeighboursAt(1, 1)) {
			System.out.println(e);
		}
	}

}
