/**
*Este archivo define la clase SimulateStateTest
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;
public class SimulatorStateTest {
	public static void main(String[] args) throws IllegalPositionException {
		Simulator s = new Simulator(10,10);
		AgentWithState outer = new AgentWithState("outer", "idle", "active"); // dos estados: idle y active
		outer.state("idle").toState("active", agent -> agent.cell().agents().size()>5);
		outer.state("active").toState("idle", agent -> agent.cell().agents().size()<=5);
		outer.state("active").addBehaviour(
			agent -> {
				List<Cell<Agent>> neighbours = agent.cell().neighbours();
				Cell<Agent> destination = neighbours.get(new Random().nextInt(neighbours.size()));
				agent.moveTo(destination);
				return true;
			}
		);
		s.create(outer, 100, 7, 7); // Crear 100 agentes "outer"
		s.run(120); // Ejecutar 60 pasos de simulacion
	}
}
