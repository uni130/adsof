/**
*Este archivo define la clase SimulatorTest
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;
import java.util.stream.Collectors;
public class SimulatorTest {
	public static void main(String[] args) throws IllegalPositionException {
		Simulator s = new Simulator(10,10);
		Agent random = new Agent("random");
		Agent outer = new Agent("outer");
		random.addBehaviour( agent -> { // se ejecuta siempre, movemos el agente a una casilla aleatoria vecina
		List<Cell<Agent>> neighbours = agent.cell().neighbours();
		Cell<Agent> destination = neighbours.get(new Random().nextInt(neighbours.size()));
		agent.moveTo(destination);
		return true;
		});
		outer.addBehaviour( agent -> agent.cell().agents().size()>5, // Lo ejecutamos si hay m�s de 5 agentes
		 // en la celda actual
		 agent -> { // nos movemos a la celda destino con menos agentes
		List<Cell<Agent>> neighbours = agent.cell().neighbours();
		Integer minAgents = neighbours.stream().
		 mapToInt( c -> c.agents().size() ).min( ).getAsInt();
		List<Cell<Agent>> destinations = neighbours.stream().
		 filter( c -> c.agents().size() == minAgents ).
		 collect(Collectors.toList());
		Cell<Agent> destination = destinations.get(new Random().nextInt(destinations.size()));
		agent.moveTo(destination);
		return true;
		});
		
		s.create(random, 100, 5, 5); // Crear 100 agentes random
		s.create(outer, 100, 7, 7); // Crear 100 agentes "outer"
		s.run(120); // Ejecutar 60 pasos de simulacion
	}
}
