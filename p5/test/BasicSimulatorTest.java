/**
*Este archivo define la clase BasicSimulatorTest
*@author Miguel Angel Luque y Miguel Diaz
*/
public class BasicSimulatorTest {

	public static void main(String[] args) throws IllegalPositionException {
		BasicSimulator s = new BasicSimulator(10,10);
		BasicAgent random = new BasicAgent("random");
		BasicAgent outer = new BasicAgent("outer");
		s.create(random, 10, 5, 5); // Crea 10 agentes de tipo random en la posici�n (5,5)
		s.create(outer, 10, 7, 7); // Crea 10 agentes de tipo outer en la posici�n (7,7)
		s.run(10);

	}

}
