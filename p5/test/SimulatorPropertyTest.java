
public class SimulatorPropertyTest {

	public static void main(String[] args) throws IllegalPositionException {
		Simulator s = new Simulator(10, 10);
		Agent ant = new Agent("ant").set("energy", 50).set("food", 0);
		Agent nest = new Agent("nest").set("food", 0);
		Agent food = new Agent("food").set("amount", 20);
		
		ant.addInteraction((self, agent) -> agent.nombre().equals("food") && agent.get("amount") > 0,
						   (self, agent) -> {
							   System.out.println("Getting food(" + agent.get("amount") + ")");
							   self.increase("energy", 10);
							   self.increase("food", 1);
							   agent.increase("amount", -1);
							   return true;
						   });
		
		s.create(ant, 3, 4, 5);
		s.create(nest, 1, 4, 5);
		s.create(ant, 5, 4, 5);
		
		s.run(10);

	}

}
