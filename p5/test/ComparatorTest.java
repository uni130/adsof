/**
*Este archivo define la clase ComparatorTest
*@author Miguel Angel Luque y Miguel Diaz
*/
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class ComparatorTest {
	private Matrix<Integer> matriz1 = null;
	private Matrix<Integer> matriz2 = null;
	private Matrix<Integer> matriz3 = null;

	@Before
	public void setUp() throws Exception {
		matriz1 = new Matrix<Integer>(4, 4);
		matriz2 = new Matrix<Integer>(4, 4);
		matriz3 = new Matrix<Integer>(4, 4);
		IMatrixElement<Integer> e1 = new MatrixElement<Integer>(3, 2, 3);
		IMatrixElement<Integer> e2 = new MatrixElement<Integer>(3, 1, 2);
		IMatrixElement<Integer> e3 = new MatrixElement<Integer>(0, 2, 1);
		matriz1.addElement(e1);
		matriz1.addElement(e2);
		matriz1.addElement(e3);
		matriz2.addElement(e1);
		matriz2.addElement(e2);
		matriz2.addElement(e3);
		matriz3.addElement(e2);
		matriz3.addElement(e3);
	}

	@Test
	public void testEquals1() {
		assertTrue(matriz1.equals(matriz2));
	}
	
	@Test
	public void testEquals2() {
		assertFalse(matriz1.equals(matriz3));
	}

	@Test
	public void testAsListSortedByColumnas() {
		List<IMatrixElement<Integer>> lista = matriz1.asListSortedBy(new ComparadorColumnas<Integer>());
		System.out.println("Por columnas:");
		for(IMatrixElement<Integer> e: lista) {
			System.out.println(e);
		}
	}
	
	@Test
	public void testAsListSortedByFilas() {
		List<IMatrixElement<Integer>> lista = matriz1.asListSortedBy(new ComparadorFilas<Integer>());
		System.out.println("Por filas:");
		for(IMatrixElement<Integer> e: lista) {
			System.out.println(e);
		}
	}

}
