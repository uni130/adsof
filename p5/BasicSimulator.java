/**
*Este archivo define la clase BasicSimulator
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;

public class BasicSimulator {
	private Matrix<List<BasicAgent>> matriz;
	
	public BasicSimulator(int cols, int rows) throws IllegalPositionException {
		int i, j;
		matriz = new Matrix<List<BasicAgent>>(cols, rows);
		
		for(i = 0;i < cols;i++) {
			for(j = 0;j < rows;j++) {
				matriz.addElement(new Cell<BasicAgent>(i, j, matriz));
			}
		}
	}
	
	/**
	*@param a agente a crear
	*@param nAgents numero de copias
	*@param i posicion eje x
	*@param j posicion eje y
	*/
	public void create(BasicAgent a, int nAgents, int i, int j) throws IllegalPositionException {
		int k;
		a.setCell((Cell<BasicAgent>)matriz.getElementAt(i, j));
		for(k = 1;k < nAgents;k++) {
			a.copy();
		}
		
	}
	
	public void print() {
		System.out.println(matriz);
	}
	
	/**
	*@param pasos numero de instrucciones a ejecutar
	*/
	public void run(int pasos) {
		int i;
		for(i = 0;i < pasos;i++) {
			System.out.println("Time = " + i);
			System.out.println(matriz);
			//TODO: que hagan su comportamiento
		}
	}
}