/**
*Este archivo define la clase Agent
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;

import java.util.function.*;
import java.util.stream.Collectors;


public class Agent extends BasicAgent implements IAgent {
	protected List<Function<IAgent, Boolean>> acciones;
	protected List<Function<IAgent, Boolean>> tacciones;
	protected List<IFunc<IAgent, IAgent, Boolean>> interacciones;
	protected List<Predicate<IAgent>> trigg;
	protected List<BiPredicate<IAgent, IAgent>> triggInteracciones;
	protected Map<String, Integer> propiedades;
	
	public Agent(String n) {
		super(n);
		acciones = new ArrayList<Function<IAgent, Boolean>>();
		tacciones = new ArrayList<Function<IAgent, Boolean>>();
		interacciones = new ArrayList<IFunc<IAgent, IAgent, Boolean>>();
		trigg = new ArrayList<Predicate<IAgent>>();
		triggInteracciones = new ArrayList<BiPredicate<IAgent, IAgent>>();
		propiedades = new TreeMap<String, Integer>();
	}

	/**
	*@param destination cell a la que ir
	*/
	@Override
	public void moveTo(Cell destination) {
		int ichange = destination.getI() - this.cell().getI();
		int jchange = destination.getJ() - this.cell().getJ();
		if((ichange <= 1 && ichange >= -1)&&(jchange <= 1 && jchange >= -1)) {
			this.setCell(destination);
		}
	}

	@Override
	public void exec() {
		Boolean continues;
		Iterator<Function<IAgent, Boolean>> iter = acciones.iterator();
		Iterator<Function<IAgent, Boolean>> iter2 = tacciones.iterator();
		Iterator<Predicate<IAgent>> iter3 = trigg.iterator();
		Iterator<IFunc<IAgent, IAgent, Boolean>> iter4 = interacciones.iterator();
		Iterator<BiPredicate<IAgent, IAgent>> iter5 = triggInteracciones.iterator();
		
		while (iter.hasNext()) {
			Function<IAgent, Boolean> accion = iter.next();
			continues = accion.apply(this);
			if(!continues) {
				iter.remove();
			}
		}
		
		while (iter2.hasNext()) {
			Function<IAgent, Boolean> accion2 = iter2.next();
			Predicate<IAgent> trigger = iter3.next();
			if(trigger.test(this)) {
				continues = accion2.apply(this);
			}else {
				continues = true;
			}
			if(!continues) {
				iter2.remove();
				iter3.remove();
			}
		}
		
		while(iter4.hasNext()) {
			continues = true;
			IFunc<IAgent, IAgent, Boolean> accion3 = iter4.next();
			BiPredicate<IAgent, IAgent> trigger2 = iter5.next();
			for(Agent a: (List<Agent>) this.cell().agents()) {
				if(trigger2.test(this, a)) {
					continues = accion3.apply(this, a);
				}else {
					continues = true;
				}
			}
			
			if(!continues) {
				iter4.remove();
			}
		}
		
		
	}
	
	/**
	*@return copia del agente
	*/
	@Override
	public IAgent copy() {
		Agent aux;
		aux = new Agent(this.nombre());
		aux.cell = this.cell;
		this.cell().addAgente(aux);;
		aux.acciones.addAll(this.acciones);
		aux.tacciones.addAll(this.tacciones);
		aux.trigg.addAll(this.trigg);
		
		for(Map.Entry<String, Integer> e: this.propiedades.entrySet()) {
			aux.propiedades.put(e.getKey(), e.getValue());
		}
		
		return aux;
	}
	
	/**
	*@param behaviour consecuencia de la condicion
	*@return agente que realiza la accion y sobre el que se comprueba la condicion
	*/
	@Override
	public IAgent addBehaviour(Function<IAgent, Boolean> behaviour) {
		this.acciones.add(behaviour);
		return this;
	}
	
	/**
	*@param trigger condicion necesaria para la ejecucion
	*@param behaviour consecuencia de la condicion
	*@return agente que realiza la accion y sobre el que se comprueba la condicion
	*/
	@Override
	public IAgent addBehaviour(Predicate<IAgent> trigger, Function<IAgent, Boolean> behaviour) {
		this.tacciones.add(behaviour);
		this.trigg.add(trigger);
		return this;
	}
	
	/**
	 * @param trigger condicion necesaria para la ejecucion
	 * @param interaction consecuencia de la condicion
	 * @return agente que realiza la accion y sobre el que se comprueba la condicion
	 */
	public IAgent addInteraction(BiPredicate<IAgent, IAgent> trigger, IFunc<IAgent, IAgent, Boolean> interaction) {
		this.interacciones.add(interaction);
		this.triggInteracciones.add(trigger);		
		return this;
	}
	
	/**
	 * Establece una propiedad del agente
	 * @param property el nombre de la propiedad
	 * @param value el valor de la propiedad
	 */
	public Agent set(String property, int value) {
		propiedades.put(property, value);
		return this;
	}
	
	/**
	 * @param key el nombre de la propiedad de la que se quiere obtener el valor
	 * @return el valor de la propiedad
	 */
	public int get(String key) {
		return propiedades.get(key);
	}
	
	/**
	 * @param property la propiedad que se quiere incrementar
	 * @param n el numero que se quiere sumar al valor de la propiedad
	 */
	public void increase(String key, int n) {
		int value = propiedades.get(key) + n;
		propiedades.put(key, value);
	}

}
