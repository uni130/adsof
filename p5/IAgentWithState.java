/**
*Este archivo define la interfaz IAgentWithState
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.function.*;

public interface IAgentWithState extends IAgent{
 IAgentState state(String name);
 IAgentState setState(String state);
 IAgentWithState addBehaviour(String state, Predicate<IAgent> trigger, Function<IAgent, Boolean> behaviour);
 IAgentWithState addBehaviour(String state, Function<IAgent, Boolean> behaviour);
}
