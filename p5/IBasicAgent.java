/**
*Este archivo define la interfaz IBasicAgent
*@author Miguel Angel Luque y Miguel Diaz
*/
public interface IBasicAgent {
	Cell cell();
	IBasicAgent copy();
	String nombre();
}
