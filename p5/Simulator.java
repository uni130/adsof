/**
*Este archivo define la clase Simulador
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;
import java.util.function.Function;
public class Simulator{
protected Matrix<List<Agent>> matriz;
	List<Agent> aa;
	public Simulator(int cols, int rows) throws IllegalPositionException {
		int i, j;
		matriz = new Matrix<List<Agent>>(cols, rows);
		
		for(i = 0;i < cols;i++) {
			for(j = 0;j < rows;j++) {
				matriz.addElement(new Cell<Agent>(i, j,matriz));
			}
		}
		aa = new ArrayList<Agent>();
	}
	
	/**
	*@param a agente a crear
	*@param nAgents numero de copias
	*@param i posicion eje x
	*@param j posicion eje y
	*/
	public void create(Agent a, int nAgents, int i, int j) throws IllegalPositionException {
		int k;
		a.setCell((Cell<Agent>)matriz.getElementAt(i, j));
		for(k = 1;k < nAgents;k++) {
			a.copy();
		}
		
	}
	
	public void print() {
		System.out.println(matriz);
	}
	
	/**
	*@param pasos numero de instrucciones a ejecutar
	*/
	public void run(int pasos) {
		int i = 0;
		Iterator<IMatrixElement<List<Agent>>> iter;
		Iterator<Agent> iter2,iter3;
		Agent a,exe;
		while(i <= pasos) {
			iter = matriz.asList().iterator();
			if(aa.size()==0) {
				while (iter.hasNext()) {
					IMatrixElement<List<Agent>> agnt = iter.next();
					if(agnt.getElement()!=null) {
						iter2 = agnt.getElement().iterator();
						while (iter2.hasNext()) {
							a = iter2.next();
							aa.add(a);
						}
					}
				}
			}
			iter3 = aa.iterator();
			while (iter3.hasNext()) {
				System.out.println("Time = " + i);
				System.out.println(matriz);
				exe = iter3.next();
				exe.exec();
				iter3.remove();
				i++;
				if(i >= pasos) {
					break;
				}
			}
		}
	}
	
}
