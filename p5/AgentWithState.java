/**
*Este archivo define la clase AgentWithState
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class AgentWithState extends Agent implements IAgentWithState{
	List<AgentState> states;
	IAgentState current;
	
	public AgentWithState(String name) {
		super(name);
		states = new ArrayList<AgentState>();
	}

	
	public AgentWithState(String name, String ... estados) {
		super(name);
		
		states = new ArrayList<AgentState>();
		
		for(String s: estados) {
			AgentState aux = new AgentState(s);
			aux.setOwner(this);
			states.add(aux);
		}
		current = states.get(0);
	}
	/**
	*@param name nombre del estado a buscar
	*@return estado con el nombre de entrada
	*/
	public IAgentState state(String name) {
		for(IAgentState state: states) {
			if(state.name().equals(name)) {
				return state;
			}
		}
		return null;
	}
	
	/**
	*@param str nombre del estado
	*@return estado actual tras el cambio
	*/
	public IAgentState setState(String str) {
		for(IAgentState state: states) {
			if(state.name().equals(str)) {
				current = state;
				return state;
			}
		}
		return current;
	}
	
	/**
	*@param str nombre del estado al que se le a�ade la expresion lambda
	*@param trigger condicion necesaria para la ejecucion
	*@param behaviour consecuencia de la condicion
	*@return agente que realiza la accion y sobre el que se comprueba la condicion
	*/
	public IAgentWithState addBehaviour(String str, Predicate<IAgent> trigger, Function<IAgent, Boolean> behaviour) {
		for(IAgentState state: states) {
			if(state.name().equals(str)) {
				return state.addBehaviour(trigger,behaviour);
			}
		}
		return null;
	}
	/**
	*@param str nombre del estado al que se le a�ade la expresion lambda
	*@param behaviour consecuencia de la condicion
	*@return agente que realiza la accion
	*/
	public IAgentWithState addBehaviour(String str, Function<IAgent, Boolean> behaviour) {
		for(IAgentState state: states) {
			if(state.name().equals(str)) {
				return state.addBehaviour(behaviour);
			}
		}
		return null;
	}
	public void exec() {
		current.changeState();
		current.exec();
	}
	/**
	*@return copia del agente
	*/
	@Override
	public IAgent copy() {
		AgentWithState aux;
		AgentState aa;
		aux = new AgentWithState(this.nombre());
		aux.cell = this.cell;
		this.cell().addAgente(aux);;
		for(AgentState actn:this.states) {
			aa = (AgentState)actn.copy();
			aa.setOwner(aux);
			aux.states.add(aa);
		}
		aux.acciones.addAll(this.acciones);
		aux.tacciones.addAll(this.tacciones);
		aux.trigg.addAll(this.trigg);
		aux.current = aux.state(this.current.name());
		return aux;
	}
	
}
