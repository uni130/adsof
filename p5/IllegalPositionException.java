/**
*Este archivo define la excepcion IllegalPositionException
*@author Miguel Angel Luque y Miguel Diaz
*/
public class IllegalPositionException extends Exception{
	public String toString(){
		return("the coordinates for the element are ilegal");
	}
}