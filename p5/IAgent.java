/**
*Este archivo define la interfaz IAgent
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.function.*;

public interface IAgent extends IBasicAgent{
	void moveTo(Cell destination); // Mover a una celda adyacente
	void exec(); // Ejecutar comportamiento del agente
	IAgent addBehaviour(Predicate<IAgent> trigger, Function<IAgent, Boolean> behaviour);
	IAgent addBehaviour(Function<IAgent, Boolean> behaviour);
	IAgent copy();
	int get(String key);
	void increase(String key, int n);
}
