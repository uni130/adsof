/**
*Este archivo define la interfaz IMatrixElement
*@author Miguel Angel Luque y Miguel Diaz
*/
public interface IMatrixElement<T> {
int getI();
int getJ();
T getElement();
void setElement(T element);
String toString();
boolean equals(Object obj);
}
