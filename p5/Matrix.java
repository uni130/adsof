/**
*Este archivo define la clase Matrix
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;

public class Matrix<T> implements IMatrix<T>{
	private int ncols;
	private int nrows;
	/*private MatrixElement<T> matriz[][];*/
	private Object matriz[][];
	
	public Matrix(int cols, int rows) {
		int i, j;
		ncols = cols;
		nrows = rows;
		matriz = new Object[ncols][nrows];
		
		for(i = 0;i < ncols;i++) {
			for(j = 0;j < nrows;j++) {
				matriz[i][j] = null;
			}
		}
	}
	
	/**
	 * @return el numero de columnas de la matriz
	 */
	public int getCols() {
		return ncols;
	}
	
	/**
	 * @return el numero de filas de la matriz
	 */
	public int getRows() {
		return nrows;
	}
	
	/**
	 * Añade un elemento a la matriz
	 * @param element el elemento que se quiere añadir
	 */
	public void addElement(IMatrixElement<T> element) throws IllegalPositionException{
		int i = element.getI();
		int j = element.getJ();
		
		if(isLegalPosition(i, j)) {
			matriz[i][j] = (MatrixElement<T>)element;
		}else{
			throw new IllegalPositionException();
		}
	}
	
	/**
	 * Comprueba si la posicion es legal
	 * @param i el numero de columna que se quiere comprobar
	 * @param el numero de fila que se quiere comprobar
	 * @return true si la posicion es legal y false si es ilegal
	 */
	public boolean isLegalPosition(int i, int j) {
		if(i < 0 || i >= ncols || j < 0 || j >= nrows) {
			return false;
		}else {
			return true;
		}
	}
	
	/**
	 * @return el elemento en la posicion (i, j)
	 * @param i el numero de columna del elemento
	 * @param j el numero de fila del elemento
	 */
	public IMatrixElement<T> getElementAt(int i, int j) throws IllegalPositionException{
		if(isLegalPosition(i, j)) {
			return (IMatrixElement<T>) matriz[i][j];
		}else{
			throw new IllegalPositionException();
		}
	}
	
	/**
	 * @return lista de todos los elementos que hay en la matriz
	 */
	public List<IMatrixElement<T>> asList(){
		int i, j;
		List<IMatrixElement<T>> lista = new ArrayList<IMatrixElement<T>>();
		
		for(i = 0;i < ncols;i++) {
			for(j = 0;j < nrows;j++) {
				if(matriz[i][j] != null){
					lista.add((IMatrixElement<T>) matriz[i][j]);
				}
			}
		}
		return lista;
	}
	
	/**
	*@param i posicion x
	*@param j posicion y
	*@return lista de vecinos
	*/
	public List<IMatrixElement<T>> getNeighboursAt(int i, int j)throws IllegalPositionException{
		int col, row;
		List<IMatrixElement<T>> lista = new ArrayList<IMatrixElement<T>>();
		
		if(!isLegalPosition(i, j)) {
			throw new IllegalPositionException();
		}
		
		//norte
		col = i - 1;
		row = j;
		if(isLegalPosition(col, row) && matriz[col][row] != null) {
			lista.add((IMatrixElement<T>) matriz[col][row]);
		}
		//sur
		col = i + 1;
		if(isLegalPosition(col, row) && matriz[col][row] != null) {
			lista.add((IMatrixElement<T>) matriz[col][row]);
		}
		//este
		col = i;
		row = j + 1;
		if(isLegalPosition(col, row) && matriz[col][row] != null) {
			lista.add((IMatrixElement<T>) matriz[col][row]);
		}
		//oeste
		row = j - 1;
		if(isLegalPosition(col, row) && matriz[col][row] != null) {
			lista.add((IMatrixElement<T>) matriz[col][row]);
		}
		
		return lista;
	}
	
	/**
	*@param obj ebjeto con el que comparar
	*@return true si son iguales
	*/
	public boolean equals(Object obj) {
		int i, j;		
		Matrix<T> aux = (Matrix<T>) obj;
		
		if(this.ncols != aux.ncols || this.nrows != aux.nrows) {
			return false;
		}		
		for(i = 0;i < this.ncols;i++) {
			for(j = 0;j < this.nrows;j++) {
				if(this.matriz[i][j] != null && aux.matriz[i][j] != null){
					if(!((MatrixElement<T>)aux.matriz[i][j]).equals((MatrixElement<T>)this.matriz[i][j])) {
						return false;
					}
				}	
				if((this.matriz[i][j] == null && aux.matriz[i][j] != null) || (this.matriz[i][j] != null && aux.matriz[i][j] == null)) {
					return false;
				}
			}
		}
		return true;		
	}
	
	/**
	*@return hashcode
	*/
	public int hashCode() {
		return this.asList().hashCode();
	}
	
	/**
	*@param c comparador a usar
	*@return lista ordenada usando el comparador
	*/
	@Override
	public List<IMatrixElement<T>> asListSortedBy(Comparator<IMatrixElement<T>> c) {
		List<IMatrixElement<T>> lista = asList();
		Collections.sort(lista, c);
		return lista;
	}	
	
	/**
	*@return matriz en forma de string
	*/
	public String toString() {
		String str = "";
		int i, j;
		
		for(j = 0;j < nrows;j++) {
			for(i = 0;i < ncols;i++) {
				if(matriz[i][j] != null) {
					str +=  String.format("%3s", ((MatrixElement<T>)matriz[i][j])) + "|";
				}else {
					str += "[ ]";
				}
			}
			str += "\n";
		}		
		
		return str;
	}

	
}
