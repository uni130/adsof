/**
*Este archivo define la clase MatrixElement
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.Comparator;

public class MatrixElement<T> implements IMatrixElement<T>{
    private int i,j;
    protected T element;
    
    
    public MatrixElement(int i, int j){
        this.i = i;
        this.j = j;
        this.element = null;
    }
    
    public MatrixElement(int i, int j,T element){
        this.i = i;
        this.j = j;
        this.element = element;
    }
    
    /**
	*@return cooedenada x
	*/
    public int getI(){
        return this.i;
    }
    
    /**
	*@return coordenada y
	*/
    public int getJ(){
        return this.j;
    }
    
    /**
	*@return elemento que lo compone
	*/
    public T getElement(){
        return element;
    }
    
    /**
	*@param element elemento que lo compone
	*/
    public void setElement(T element){
        this.element = element;
    }
    
    /**
	*@param obj objeto a comparar
	*@return true if equals
	*/
    public boolean equals(Object obj){
        if(obj==this)
            return true;
        if(!(obj instanceof MatrixElement))
            return false;
        MatrixElement<T> me = (MatrixElement)obj;
        return me.i == this.i && me.j == this.j && me.element.equals(this.element);
    }
    
    /**
	*@return elemento como cadena
	*/
    public String toString(){
        return ("" + element);
    }
}
