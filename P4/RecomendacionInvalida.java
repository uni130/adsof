class RecomendacionInvalida extends Exception{
	Recomendacion recomendacion;
    public RecomendacionInvalida(Recomendacion r) {
    	recomendacion = r;
    }
    public String toString () {
        return("La recomendacion " + recomendacion +" es invalida");
    }
}
