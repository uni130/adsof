import java.util.*;
import java.io.*;

public interface ModeloDatosInterfaz {
    public void leeFicheroPreferencias(String ruta)throws FileNotFoundException, IOException;
    public Map<Long, Double> getPreferenciasUsuario(Long usuario);
    public Map<Long, Double> getPreferenciasItem(Long item);
    public Set<Long> getUsuariosUnicos();
    public Set<Long> getItemsUnicos();
    //Otros metodos que los/las estudiantes crean oportuno
}
