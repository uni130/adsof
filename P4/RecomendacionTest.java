import java.io.*;
import java.util.*;

public class RecomendacionTest {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		int numRecomendaciones = 0;
		int numRecomendacionesAleat = 0;
		int numRecomendacionesPopu = 0;
		int i = 0;
		double suma = 0;
		double suma2 = 0;
		double sumaAleat = 0;
		double suma2Aleat = 0;
		double sumaPopu = 0;
		double suma2Popu = 0;
		double aux = 0;
		double aux2 = 0;
		double aux3 = 0;
		double aux4 = 0;
		double aux5 = 0;
		double aux6 = 0;
		Set<Long> usuarios;
		Recomendacion r = null;
		Recomendacion r2 = null;
		Recomendacion r3 = null;
		List<Recomendacion> recomendaciones = new ArrayList<>();
		List<Recomendacion> recomendacionesAleat = new ArrayList<>();
		List<Recomendacion> recomendacionesPopu = new ArrayList<>();
		ModeloDatos m = ModeloDatos.getInstance();
		Precision precision = new Precision(m, 3);
		Recall recall = new Recall(m, 3);
		m.leeFicheroPreferencias("PruebaTraining.txt");
		usuarios = m.getUsuariosUnicos();
		for(Long u: usuarios){
			try{
				r = new Recomendacion(u);
				recomendaciones.add(r.recomiendaVecinos(100));
			}catch(RecomendacionInvalida ri){
				if(r != null){
					Recomendacion.getAllRecomendaciones().remove(r);
					recomendaciones.remove(r);
				}
			}

			try{
				r2 = new Recomendacion(u);
				recomendacionesAleat.add(r2.recomiendaAleatorio());
			}catch(RecomendacionInvalida ri){
				if(r2 != null){
					Recomendacion.getAllRecomendaciones().remove(r2);
					recomendacionesAleat.remove(r2);
				}
			}

			try{
				r3 = new Recomendacion(u);
				recomendacionesPopu.add(r3.recomiendaPopularidad());
			}catch(RecomendacionInvalida ri){
				if(r3 != null){
					Recomendacion.getAllRecomendaciones().remove(r3);
					recomendacionesPopu.remove(r3);
				}
			}


		}
		m.leeFicheroPreferencias("PruebaTraining.txt");
		for(Recomendacion rec: recomendaciones){
				try{
						aux = precision.evalua(rec,5);
						aux2 = recall.evalua(rec,5);
				}catch(UsuarioNoRelevante ue){
						suma -= aux;
						suma2 -= aux2;
						numRecomendaciones--;
				}
				try{
					aux3 = precision.evalua(recomendacionesAleat.get(i), 5);
					aux4 = recall.evalua(recomendacionesAleat.get(i), 5);
				}catch(UsuarioNoRelevante ue){
					sumaAleat -= aux3;
					suma2Aleat -= aux4;
					numRecomendacionesAleat--;
				}
				try{
					aux5 = precision.evalua(recomendacionesPopu.get(i), 5);
					aux6 = recall.evalua(recomendacionesPopu.get(i), 5);
				}catch(UsuarioNoRelevante ue){
					sumaPopu -= aux5;
					suma2Popu -= aux6;
					numRecomendacionesPopu--;
				}
				suma += aux;
				suma2 += aux2;
				numRecomendaciones++;
				sumaAleat += aux3;
				suma2Aleat += aux4;
				numRecomendacionesAleat++;
				sumaPopu += aux5;
				suma2Popu += aux6;
				numRecomendacionesPopu++;
				i++;
		}
		suma = suma/numRecomendaciones;
		suma2 = suma2/numRecomendaciones;
		sumaAleat = sumaAleat/numRecomendacionesAleat;
		suma2Aleat = suma2Aleat/numRecomendacionesAleat;
		sumaPopu = sumaPopu/numRecomendacionesPopu;
		suma2Popu = suma2Popu/numRecomendacionesPopu;
		System.out.println("la media de metrica de precision es: " + suma + " en recomiendaVecinos");
		System.out.println("la media de metrica de recall es: " + suma2 + " en recomiendaVecinos");
		System.out.println("la media de metrica de precision es: " + sumaAleat + " en recomiendaAleatorio");
		System.out.println("la media de metrica de recall es: " + suma2Aleat + " en recomiendaAleatorio");
		System.out.println("la media de metrica de precision es: " + sumaPopu + " en recomiendaPopularidad");
		System.out.println("la media de metrica de recall es: " + suma2Popu + " en recomiendaPopularidad");
	}
}
