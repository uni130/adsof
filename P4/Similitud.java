/**
*Esta clase define la interfaz Similitud
*@author Miguel Ángel Luque y Miguel Díaz
*/

public interface Similitud{
    public double sim(long u1, long u2);
}
