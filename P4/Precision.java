/**
*Este archivo implementa los metodos de metrica segun el criterio precision
*@author Miguel Angel Luque y Miguel Diaz
*/
import java.util.*;

class Precision implements Metrica {
    ModeloDatos modelo;
    double notaMinima;//La nota minima que tiene que tener un usuario para ser relevante

    public Precision(ModeloDatos m, double n){
        modelo = m;
        notaMinima = n;
    }

    /**
    *@param rec recomendation to test
    *@param n number of items extracted from the recomendation
    *@return double between 1 and 0 with the precision of the recomdation by the class criteria
    */
    public double evalua(Recomendacion rec, int n) throws UsuarioNoRelevante{
        Set<Long> selected = new TreeSet<>();
        Set<Long> selected2 = getItemsRelevantes(rec.getUsuario());
        int i = 0;
        for(Tupla item: rec.getRecomendaciones()) {
            selected.add(item.getItem());
            i++;
            if(i == n){
                break;
            }
        }
        if(selected.isEmpty()||selected2.isEmpty()){
            throw new UsuarioNoRelevante(rec.getUsuario());
        }
        selected.retainAll(selected2);//interseccion de ambos set
        
        return (double)selected.size()/n;

    }

    /**
    *@param u user extract the valorations
    *@return set with the items more relevant than the minumum limit
    */
    public Set<Long> getItemsRelevantes(long u){
        Set<Long> items = new TreeSet<>();

        for(Map.Entry<Long, Double> pref: ModeloDatos.getInstance().getPreferenciasUsuario(u).entrySet()){
            if((Double)pref.getValue() >= notaMinima){
                items.add((Long)pref.getKey());
            }
        }

        return items;
    }
}
