/**
*Esta clase define el objeto Tupla
*@author Miguel Ã�ngel Luque y Miguel DÃ­az
*/

public class Tupla implements Comparable<Tupla>{
    private long item;
    private double score;

    public Tupla(long i, double s){
        item = i;
        score = s;
    }

    /**
    *@return el identificador del item
    */
    public long getItem(){
        return item;
    }

    /**
    *@return el valor que se le ha asignado al item
    */
    public double getScore(){
        return score;
    }

    public String toString(){
        return "Tupla  [elemento=" + item + ",  valor=" + score + "]";
    }

	@Override
	public int compareTo(Tupla t) {
		if(this.score < t.score) {
			return 1;
		}else if(this.score == t.score){
			return 0;
		}else {
			return -1;
		}
	}
}
