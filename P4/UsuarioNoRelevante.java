class UsuarioNoRelevante extends Exception{
    private long usuario;
    public UsuarioNoRelevante(long num) {
        usuario = num;
    }
    public String toString () {
        return("El usuario " + usuario + " no es relevante");
    }
}
