/**
*Esta clase define la interfaz Recomendador
*@author Miguel Ã�ngel Luque y Miguel DÃ­az
*/

public interface Recomendador{
	public Recomendacion recomienda(long u, int longitudRecomendacion) throws RecomendacionInvalida;
    public Recomendacion recomiendaVecinos(int longitudRecomendacion) throws RecomendacionInvalida;
    public Recomendacion recomiendaAleatorio() throws RecomendacionInvalida;
    public Recomendacion recomiendaPopularidad() throws RecomendacionInvalida;
}
