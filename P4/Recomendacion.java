/**
*Esta clase define el objeto Recomendacion
*@author Miguel Ã�ngel Luque y Miguel DÃ­az
*/

import java.io.*;
import java.util.*;

public class Recomendacion implements Recomendador, Similitud{
    private long usuario;
    private List<Tupla> recomendaciones = new ArrayList<Tupla>();
    private static List<Recomendacion> allRecomendaciones = new ArrayList<Recomendacion>();
    ModeloDatos modelo;

    public Recomendacion(){
        modelo = ModeloDatos.getInstance();
    }
    public Recomendacion(long usuario){
    	this.usuario = usuario;
        modelo = ModeloDatos.getInstance();
    }

    public void addRecomendacion(Tupla t){
        recomendaciones.add(t);
    }
    public Recomendacion recomienda(long u, int longitudRecomendacion) throws RecomendacionInvalida{
    	usuario = u;
    	int l;

    	recomiendaVecinos(2);//el argumento es el numero de vecinos
    	//recomiendaAleatorio();
    	//recomiendaPopularidad();


    	if(longitudRecomendacion > recomendaciones.size()) {
    		l = recomendaciones.size();
    	}else {
    		l = longitudRecomendacion;
    	}

    	recomendaciones = recomendaciones.subList(0, l);
    	return this;
    }

    /**
    *Hace una recomendacion a un usuario con longitudRecomendacion vecinos
    *@param u el usuario al que se quiere hacer la recomendacion
    *@param longitudRecomendacion el numero de vecinos con los que se quiere hacer la recomendacion
    *@return la recomendacion que se le hace al usuario
    */
    public Recomendacion recomiendaVecinos(int nvecinos) throws RecomendacionInvalida{
        int i = 0;
        double score = 0;
        int k;
        //set de usuarios vecinos
        Set<Long> usuarios = modelo.getUsuariosUnicos();
        //set de items
        Set<Long> items = modelo.getItemsUnicos();
        //tienen que estar ordenados por similitud, por lo que es la clave
        SortedMap<Double, Long> vecinos = new TreeMap<Double, Long>(Collections.reverseOrder());
        Map<Long, Double> prefs = modelo.getPreferenciasUsuario(usuario);
        Map<Long, Double> aux;

        //ordenar los vecinos de mayor a menor similitud
        for(long usr: usuarios){
        	if(usr != usuario) {//Para que no calcule la similitud con él mismo
        		vecinos.put(sim(usuario, usr), usr);
        	}
        }
        if(nvecinos <= 0){
            k = 5;
        }else{
            k = nvecinos;
        }
        //calcular el score del item con los longitudRecomendacion primeros
        for(long c: items){//por cada item
        	i = 0;
            for(Map.Entry<Double, Long> v: vecinos.entrySet()){//por cada vecino hasta longitudRecomendacion
                if(i < k){
                    aux = modelo.getPreferenciasUsuario((long)v.getValue());
                    for(Map.Entry<Long, Double> t: aux.entrySet()){//por cada preferencia del vecino
                        if((long)t.getKey() == c){//hacerlo solo con el item c en caso de que lo halla puntuado
                            score += ((double)t.getValue() * (double)v.getKey());//score += rating*sim(u, v)
                        }
                    }
                }
                i++;
            }
            if(score != 0 && !prefs.containsKey(c)) {//comprueba que el score no sea 0 y que el usuario no haya puntuado el objeto
            	this.addRecomendacion(new Tupla(c, score));//añadir tupla del item c
            }
            score = 0;//volver a poner el score a 0 para el siguiente item
        }

        Collections.sort(recomendaciones);
        allRecomendaciones.add(this);
        return this;
    }

    /**
    *Calcula la similitud entre dos usuarios
    *@param u1 el usuario u
    *@param u2 el usuario v
    *@return la similitud entre los usuarios u y v
    */
    public double sim(long u1, long u2){
        double ruv = 0;//variable del numerador
        //variables del denominador
        double ru = 0;
        double rv = 0;
        //variable del resultado
        double resultado;
        Map<Long, Double> prefu1 = modelo.getPreferenciasUsuario(u1);
        Map<Long, Double> prefu2 = modelo.getPreferenciasUsuario(u2);

        //calcular numerador (sumatorio de los ratings de los items que han puntuado u y v)
        for(Map.Entry<Long, Double> m1: prefu1.entrySet()){
            for(Map.Entry<Long, Double> m2: prefu2.entrySet()){
                if(m1.getKey() == m2.getKey()){
                    ruv += ((double)m1.getValue() * (double)m2.getValue());
                }
            }
        }

        //calcular denominador
        //sumatorio del cuadrado de los ratings de u
        for(Map.Entry<Long, Double> m1: prefu1.entrySet()){
            ru += ((double)m1.getValue() * (double)m1.getValue());
        }
        //sumatorio del cuadrado de los ratings de v
        for(Map.Entry<Long, Double> m2: prefu2.entrySet()){
            rv += ((double)m2.getValue() * (double)m2.getValue());
        }

        //raiz cuadrada de la multiplicacion de los sumatorios
        resultado = Math.sqrt(ru * rv);

        //calcular el resultado final de la similitud
        resultado = ruv/resultado;

        return resultado;
    }

    /**
    *Hace una recomendacion aleatoria a un usuario
    *@return la recomendacion que se le hace al usuario
    */
    public Recomendacion recomiendaAleatorio() throws RecomendacionInvalida{
        Set<Long> items = modelo.getItemsUnicos();
        Map<Long, Double> prefs = modelo.getPreferenciasUsuario(usuario);
        for(long c: items){
        	if(!prefs.containsKey(c)) {
        		this.addRecomendacion(new Tupla(c, Math.random()*5));
        	}
        }
        Collections.sort(recomendaciones);
        allRecomendaciones.add(this);
        return this;
    }

    /**
    *Hace una recomendacion por popularidad a un usuario
    *@return la recomendacion que se le hace al usuario
    */
    public Recomendacion recomiendaPopularidad() throws RecomendacionInvalida{
        double score;
        Set<Long> items = modelo.getItemsUnicos();
        Map<Long, Double> aux;
        Map<Long, Double> prefs = modelo.getPreferenciasUsuario(usuario);

        for(long item: items){
            aux = modelo.getPreferenciasItem(item);
            score = aux.size();//El tamaÃ±o es el numero de veces que han sido puntuados
            if(!prefs.containsKey(item)) {
            	addRecomendacion(new Tupla(item, score));
        	}
        }
        Collections.sort(recomendaciones);
        allRecomendaciones.add(this);
        return this;
    }

    /**
    *Guarda todas las recomendaciones que se han hecho en un fichero
    */
    public static void guardaRecomendaciones() throws IOException{
    	Writer escritor = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("recomendaciones.txt"), "utf-8"));
		for(Recomendacion r: allRecomendaciones) {
			for(Tupla t: r.recomendaciones) {
				escritor.write(r.usuario + "    "+ t.getItem() + "  " + t.getScore() + "\n");
			}
		}
		escritor.close();
	}

  /**
  *Carga todas las recomendaciones que se han hecho en un fichero
  */
    public static void cargaRecomendaciones() throws IOException {
    	String linea;
    	long item;
    	long user;
    	long prevUser = -1;
    	double score;
    	StringTokenizer tokens;
    	Recomendacion r = null;
    	FileInputStream stream = new FileInputStream("recomendaciones.txt");
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader buffer = new BufferedReader(reader);

        while((linea = buffer.readLine()) != null) {
        	tokens = new StringTokenizer(linea);
        	user = Long.parseLong(tokens.nextToken());
        	item = Long.parseLong(tokens.nextToken());
        	score = Double.parseDouble(tokens.nextToken());

        	if(user != prevUser) {
        		r = new Recomendacion();
        		r.usuario = user;
        		r.addRecomendacion(new Tupla(item, score));
        		allRecomendaciones.add(r);
        	}else {
        		if(r != null) {
        			r.addRecomendacion(new Tupla(item, score));
        		}
        	}
        	prevUser = user;
        }
		buffer.close();
	}

    /**
     *@return Todas las recomendaciones que se han hecho
     */
    public static List<Recomendacion> getAllRecomendaciones(){
    	return allRecomendaciones;
    }

    /**
     *@return lista de recomendaciones concretas para el usuario
     */
     public List<Tupla> getRecomendaciones(){
    	 return recomendaciones;
     }

     /**
     *@return usuario al cual se le hacen las recomendaciones
     */
     public long getUsuario(){
    	 return usuario;
     }

    public String toString(){
        String r;
        r = "Recomendacion del usuario " + usuario + ": \n";

        for(int i = 0;i < recomendaciones.size();i++){
            r = r + recomendaciones.get(i) + "\n";
        }

        return r;
    }


}
