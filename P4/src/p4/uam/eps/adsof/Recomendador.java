/**
*Esta clase define la interfaz Recomendador
*@author Miguel Angel Luque y Miguel Diaz
*/
package p4.uam.eps.adsof;
public interface Recomendador{
	public Recomendacion recomienda(long u, int longitudRecomendacion) throws RecomendacionInvalida;
    public Recomendacion recomiendaVecinos(int longitudRecomendacion) throws RecomendacionInvalida;
    public Recomendacion recomiendaAleatorio() throws RecomendacionInvalida;
    public Recomendacion recomiendaPopularidad() throws RecomendacionInvalida;
}
