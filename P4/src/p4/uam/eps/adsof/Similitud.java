/**
*Esta clase define la interfaz Similitud
*@author Miguel Angel Luque y Miguel Diaz
*/
package p4.uam.eps.adsof;
public interface Similitud{
    public double sim(long u1, long u2);
}
