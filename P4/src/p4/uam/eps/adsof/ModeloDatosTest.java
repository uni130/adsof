/**
*Este archivo define el test de ModeloDatos
*@author Miguel Angel Luque y Miguel Diaz
*/
package p4.uam.eps.adsof;

import java.io.*;
import java.util.*;

public class ModeloDatosTest{
    public static void main(String[] args){
        Set<Long> ids;
        Map<Long, Double> aux;
        ModeloDatos m = ModeloDatos.getInstance();

        try{
            m.leeFicheroPreferencias("PruebaTraining.txt");
        }catch(IOException e){

        }

        ids = m.getUsuariosUnicos();

        System.out.println("Usuarios:");
        for(long id: ids){
            System.out.println(id);
        }

        ids = m.getItemsUnicos();

        System.out.println("Items:");
        for(long id: ids){
            System.out.println(id);
        }

        for(long id: m.getUsuariosUnicos()){
            aux = m.getPreferenciasUsuario(id);
            System.out.println("Preferencias del usuario " + (id) + ":");
            for(Map.Entry<Long, Double> a: aux.entrySet()){
                System.out.println(a.getKey() + ", " + a.getValue());
            }
        }

        for(long id: m.getItemsUnicos()){
            aux = m.getPreferenciasItem(id);
            System.out.println("Preferencias del item " + id + ":");
            for(Map.Entry<Long, Double> a: aux.entrySet()){
                System.out.println(a.getKey() + ", " + a.getValue());
            }
        }
    }
}
