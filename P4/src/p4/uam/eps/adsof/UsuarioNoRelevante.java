/**
*Esta clase define la excepcion UsuarioNoRelevante
*@author Miguel Angel Luque y Miguel Diaz
*/
package p4.uam.eps.adsof;
class UsuarioNoRelevante extends Exception{
    private long usuario;
    public UsuarioNoRelevante(long num) {
        usuario = num;
    }
    public String toString () {
        return("El usuario " + usuario + " no es relevante");
    }
}
