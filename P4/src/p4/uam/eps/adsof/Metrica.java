/**
*Este archivo define la interfaz de Metrica
*@author Miguel Angel Luque y Miguel Diaz
*/

package p4.uam.eps.adsof;
import java.util.*;
public interface Metrica{
    public double evalua(Recomendacion rec, int n)throws UsuarioNoRelevante;
    public Set<Long> getItemsRelevantes(long u);
}
