/**
*Esta clase define la excepcion Recomendacioninvalida
*@author Miguel Angel Luque y Miguel Diaz
*/
package p4.uam.eps.adsof;
class RecomendacionInvalida extends Exception{
	Recomendacion recomendacion;
    public RecomendacionInvalida(Recomendacion r) {
    	recomendacion = r;
    }
    public String toString () {
        return("La recomendacion " + recomendacion +" es invalida");
    }
}
