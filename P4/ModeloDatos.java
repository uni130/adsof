/**
*Este archivo define el singleton ModeloDatos
*@author Miguel Angel Luque y Miguel Diaz
*/

import java.io.*;
import java.util.*;

public class ModeloDatos implements ModeloDatosInterfaz{
    private Map<Long, Map<Long, Double>> preferencias = null;

    private FileInputStream stream;
    private InputStreamReader reader;
    private BufferedReader buffer;

    private StringTokenizer tokens;
    private String linea;

    private static ModeloDatos modelo;

    private ModeloDatos(){
        preferencias = new TreeMap<>();
    }
    public static ModeloDatos getInstance() {
       if(modelo == null) {
           modelo = new ModeloDatos();
       }

       return modelo;
   }

   /**
   *@param ruta : fichero en el que leer los datos de entrada
   */
    public void leeFicheroPreferencias(String ruta)throws FileNotFoundException, IOException{
        Map<Long, Double> tmp = null;
        long usuario = -1;
        long usuariotmp = -1;
        Long item;
        double puntuacion;
        if(!preferencias.isEmpty()) {
        	preferencias.clear();
        }
        /*Abre el fichero de preferencias*/
        try{
        stream = new FileInputStream(ruta);
        reader = new InputStreamReader(stream);
        buffer = new BufferedReader(reader);

        /*Carga las posadas*/
        while ((linea = buffer.readLine()) != null) {
            tokens = new StringTokenizer(linea);
            usuariotmp = Long.parseLong(tokens.nextToken());
            if(usuariotmp != usuario){
              if((usuario != -1)&&(tmp != null)){
                preferencias.put(usuario,tmp);
              }
              usuario = usuariotmp;
              tmp = new TreeMap<>();
            }
            if(tokens.hasMoreTokens()){
                item = Long.parseLong(tokens.nextToken());
                if(tokens.hasMoreTokens()){
                    puntuacion = Double.parseDouble(tokens.nextToken());
                    tmp.put(item,puntuacion);
                }
            }
        }if(usuario != -1){
            preferencias.put(usuario,tmp);
        }
    buffer.close();
    }
    catch(FileNotFoundException e){throw e;}
    catch(IOException e){throw e;}
  }

    /**
    *@param usuario del la cual devolver las preferencias
    *@return preferencias del usuario introducido
    */
    public Map<Long, Double> getPreferenciasUsuario(Long usuario){
        return preferencias.get(usuario);
    }

    /**
    *@param item del la cual devolver las preferencias
    *@return preferencias del item indicado
    */
    public Map<Long, Double> getPreferenciasItem(Long item){
        Map<Long, Double> m = new TreeMap<>();
        Map<Long, Double> v;
        double g = -1;
        for(Long i: preferencias.keySet()){
            v = preferencias.get(i);
            for(Map.Entry<Long, Double> mapa: v.entrySet()){
                if(mapa.getKey() == item){
                    g = (double)mapa.getValue();
                    m.put(i,g);
                }
            }
        }
        return m;
    }

    /**
    *@return usuarios en la base de datos
    */
    public Set<Long> getUsuariosUnicos(){
        return preferencias.keySet();
    }

    /**
    *@return items en la base de datos
    */
    public Set<Long> getItemsUnicos(){
        Set<Long> s = new TreeSet<>();
        for(Map<Long, Double> v: preferencias.values()){
            for(Long w: v.keySet()){
                s.add(w);
            }
        }
        return s;
    }
}
