/**
*Esta clase define el objeto Camino
*@author Miguel Díaz y Miguel Ángel Luque
*/
import java.util.*;
public class Camino{
  protected Posada origen;
  protected Posada destino;
  protected int coste;
  protected static int total = 0;
  protected boolean trampa = false;
  /**
  *@param o la posada que se quiere poner como origen
  *@param d la posada que se quiere poner como destino
  *@param c el coste que tiene el camino
  */
  public Camino(Posada o, Posada d, int c){
    origen = o;
    destino = d;
    if(c < 1){
      coste = 1;
    }else{
      coste = c;
    }
    total += coste;
  }

  /**
  *@return Posada origen del camino
  */
  public Posada getOrigen(){
    return origen;
  }

  /**
  *@return Posada destino del camino
  */
  public Posada getDestino(){
    return destino;
  }

  /**
  *@return Posada destino real del camino
  */
  public Posada getDestinoReal(){
    return destino;
  }

  /**
  *@return entero con el coste del camino
  */
  public int getCoste(){
    return coste;
  }

  /**
  *Cambia el destino y el coste del camino
  *@param d la posada que se quiere poner como destino
  *@param c el nuevo coste que tiene el camino
  */
  public void cambiarDestino(Posada d, int c){
    destino = d;

    total -= coste;
    if(c < 1){
      coste = 1;
    }else{
      coste = c;
    }

    total += coste;
  }

  /**
  *@return cadena para imprimir el camino
  */
  public String toString(){
    return "(" + origen.getNombre() + "--" + coste + "-->" + destino.getNombre() + ")";
  }

  /**
  *@return suma del coste de todos los caminos
  */
  public static int getTotal(){
    return total;
  }

  /**
  *@return entero con el coste real del camino
  */
  public int costeReal(){
    return coste;
  }

  /**
  *@return true si el camino es una trampa, false si no lo es
  */

  public boolean esTrampa(){
    return trampa;
  }

}
