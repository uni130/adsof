public class EjemploDeUsoExploradoresBasicos {
  public static void main(String[] args) {
    Posada solana = new Posada("Solana", 1);
    Posada romeral = new Posada("Romeral", 5);
    Posada tomelloso = new Posada("Tomelloso"); // por defecto energía recuperada 2
    Explorador sancho = new Explorador("Sancho", 200, solana);

    if(!solana.addCamino(new Camino(solana, tomelloso, 33))){
      System.out.println("Error en addCamino");
    }
    System.out.println(sancho);
    if(!sancho.recorre(romeral, tomelloso)){//irá directo a tomelloso sin pasar por romeral
      System.out.println("No ha recorrido todos los caminos");
    }
    System.out.println(sancho);

    if(!tomelloso.addCamino(new Camino(tomelloso, romeral, 20))){
      System.out.println("Error en addCamino");
    }

    tomelloso.getCamino(romeral).cambiarDestino(solana, 2);//Cambiamos el destino para que pueda ir a solana

    if(!sancho.recorre(solana)){//Ahora debería pòder llegar a  solana
      System.out.println("No ha recorrido todos los caminos");
    }

    System.out.println(sancho);
    sancho.recorre(tomelloso);//Deberia poder ir a tomelloso
    System.out.println(sancho);
    tomelloso.addCamino(new Camino(tomelloso, romeral, 11));
    sancho.recorre(tomelloso.getCamino(romeral));//deberia poder ir a romeral
    System.out.println(sancho);
    System.out.println(Camino.getTotal());//33 + 20 - 20 + 2 + 11 = 46
  }
}
