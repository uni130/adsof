/**
*Esta clase define el objeto Simulacion
*@author Miguel Ángel Luque y Miguel Díaz
*/

import java.io.*;
import java.util.*;

public class Simulacion{
  private List<Posada> listaPosadas = new ArrayList<Posada>();
  private List<Camino> listaCaminos = new ArrayList<Camino>();
  private List<Explorador> listaExploradores = new ArrayList<Explorador>();
  private Explorador e = null;

  private FileInputStream stream;
  private InputStreamReader reader;
  private BufferedReader buffer;

  private StringTokenizer tokens;
  private String linea;

  private Posada origen = null, destino = null;
  private String o, d;

  /**
  *Carga la lista de posadas
  *@param posadas el path del fichero con la informacion de las posadas
  */
  private void cargaPosadas(String posadas) throws IOException{
    String nombre;
    int recarga;
    /*Abre el fichero de las posadas*/
    stream = new FileInputStream(posadas);
    reader = new InputStreamReader(stream);
    buffer = new BufferedReader(reader);
    /*Carga las posadas*/
    while ((linea = buffer.readLine()) != null) {
      tokens = new StringTokenizer(linea);
      nombre = tokens.nextToken();
      if(tokens.hasMoreTokens()){
        recarga = Integer.parseInt(tokens.nextToken());
        if(tokens.hasMoreTokens()){
          listaPosadas.add(new Posada(nombre, recarga, Posada.Luz.valueOf(tokens.nextToken())));
        }else{
          listaPosadas.add(new Posada(nombre, recarga));
        }
      }else{
        listaPosadas.add(new Posada(nombre));
      }
    }

    buffer.close();
  }

  /**
  *Carga la lista de caminos
  *@param caminos el path del fichero con la informacion de los caminos
  */
  private void cargaCaminos(String caminos) throws IOException{
    int coste;
    float extra, probabilidad;
    //Si es una trampa se añaden dos campos mas: extra y probabilidad separados por espacios en la misma linea
    /*Abre el fichero de los caminos*/
    stream = new FileInputStream(caminos);
    reader = new InputStreamReader(stream);
    buffer = new BufferedReader(reader);

    /*Carga los caminos*/
    while ((linea = buffer.readLine()) != null) {
      tokens = new StringTokenizer(linea);
      o = tokens.nextToken();
      d = tokens.nextToken();
      /*bucle para buscar las posadas origen y destino del camino*/
      for(Posada p: listaPosadas){
        if(o.equals(p.getNombre())){
          origen = p;
        }
        if(d.equals(p.getNombre())){
          destino = p;
        }
      }
      coste = Integer.parseInt(tokens.nextToken());

      //Si es una trampa (tiene mas campos en el fichero) se crea una trampa
      if(tokens.hasMoreTokens()){
        extra = Float.parseFloat(tokens.nextToken());
        probabilidad = Float.parseFloat(tokens.nextToken());
        listaCaminos.add(new Trampa(origen, destino, coste, extra, probabilidad));
      }else{
        listaCaminos.add(new Camino(origen, destino, coste));
      }
    }
    buffer.close();
    /*Añade los caminos a las posadas*/
    for(Posada p: listaPosadas){
      for(Camino c: listaCaminos){
        p.addCamino(c);
      }
    }
  }

  /**
  *Carga los exploradores
  *@param explorador el path del fichero con la informacion del explorador
  */
  private void cargaExploradores(String explorador) throws IOException{
    int n;
    String nombre;
    String clase;

    /*Abre el fichero del explorador*/
    stream = new FileInputStream(explorador);
    reader = new InputStreamReader(stream);
    buffer = new BufferedReader(reader);

    /*Carga los exploradores*/
    while((linea = buffer.readLine()) != null){
      tokens = new StringTokenizer(linea);
      nombre = tokens.nextToken();
      //Comprueba que no este en la parte de movimientos
      if(tokens.hasMoreTokens()){
        n = Integer.parseInt(tokens.nextToken());
        o = tokens.nextToken();

        for(Posada p: listaPosadas){
          if(o.equals(p.getNombre())){
            origen = p;
          }
        }

        if(tokens.hasMoreTokens()){
          clase = tokens.nextToken();
          if(clase.equals("Hada")){
            listaExploradores.add(new Hada(nombre, n, origen, Integer.parseInt(tokens.nextToken())));
          }else if(clase.equals("Hechicero")){
            listaExploradores.add(new Hechicero(nombre, n, origen, Integer.parseInt(tokens.nextToken())));
          }
        }else{
          listaExploradores.add(new Explorador(nombre, n, origen));
        }
      }
    }
  }

  /**
  *Realiza una simulacion de 3 ficheros de texto
  *@param posadas El path del fichero con la informacion de las posadas
  *@param caminos El path del fichero con la informacion de los caminos
  *@param explorador el path del fichero con la informacion del explorador
  */
  public Simulacion(String posadas, String caminos, String explorador) throws IOException{
    int i;
    /*Carga las listas de posadas y caminos y el explorador*/
    cargaPosadas(posadas);
    cargaCaminos(caminos);
    cargaExploradores(explorador);

    /*Abre el fichero del explorador*/
    stream = new FileInputStream(explorador);
    reader = new InputStreamReader(stream);
    buffer = new BufferedReader(reader);

    //Los exploradores recorren los caminos por turnos
    for(i = 0;(linea = buffer.readLine()) != null;i++){
      tokens = new StringTokenizer(linea);
      tokens.nextToken();
      //Comprueba que no esta en la parte de exploradores
      if(!tokens.hasMoreTokens()){
        for(Posada p: listaPosadas){
          if(linea.equals(p.getNombre())){
            destino = p;
          }
        }
        listaExploradores.get(i % listaExploradores.size()).recorre(destino);
        System.out.println(listaExploradores.get(i % listaExploradores.size()));
      }
    }

    buffer.close();
  }
}
