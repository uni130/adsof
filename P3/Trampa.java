/**
*Esta clase define el bjeto Camino
*@author Miguel Díaz y Miguel Ángel Luque
*/
import java.util.*;
public class Trampa extends Camino{
  private float extra;
  private float probabilidad;

  /**
  *@param o la posada que se quiere poner como origen
  *@param d la posada que se quiere poner como destino
  *@param c el coste que tiene el camino
  *@param extra multiplicador a coste del camino al ser una trampa
  *@param probabilidad probabilidad de que se vuelva al origen
  */
  public Trampa(Posada o, Posada d, int c,float extra,float probabilidad){
    super(o,d,c);
    this.extra = extra;
    this.probabilidad = probabilidad;
    trampa = true;
  }

  /**
  *@return entero con el coste especial del camino
  */
  public int costeEspecial(){
    return (int)(this.getCoste() * extra);
  }

  /**
  *@return entero con el coste real del camino
  */
  public int costeReal(){
    return (coste + this.costeEspecial());
  }

  /**
  *@return Posada que es el destino real del camino
  */
  public Posada getDestinoReal(){
    if((Math.random() * 1) < probabilidad){
      return origen;
    }else{
      return destino;
    }
  }
}
