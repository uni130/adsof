/**
*Esta clase define el objeto Hechicero
*@author Miguel Díaz y Miguel Ángel Luque
*/
import java.util.*;
public class Hechicero extends Mago{

  /**
  *@param n el nombre del explorador
  *@param e la energia que tiene el explorador
  *@param u la posada en la que se encuentra el explorador al empezar
  *@param p poder del mago
  */
  public Hechicero(String n, int e, Posada u, int p){
    super(n,e,u,p);
  }

  /**
  *@return boolean true si el hechicero se puede alojar en la posada p
  *@param Posada p la posada en la que se quiere comprobar si el hechicero se puede alojar
  */
  public boolean puedeAlojarseEn(Posada p){
    if(p.getLuz().ordinal()<=(poder+Posada.Luz.TENEBROSA.ordinal()) && !p.tieneHada()){
      return true;
    }else{
      return false;
    }
  }
}
