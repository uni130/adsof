/**
*Esta clase ejecuta una serie de pruebas para comprobar la funcionalidad de las clases
*@author Miguel Díaz y Miguel Ángel Luque
*/
import java.util.*;
public class ProgramaDePruebas {
  public static void main(String[] args) {
    Posada solana = new Posada("Solana", 1);
    Posada romeral = new Posada("Romeral", 5);
    Posada rohan = new Posada("Rohan", 5);
    Posada tomelloso = new Posada("Tomelloso", 1, Posada.Luz.TENEBROSA); // por defecto energía recuperada 2
    Posada ponipisador = new Posada("PoniPisador", 10, Posada.Luz.TENEBROSA);
    Posada elfoalegre = new Posada("ElfoAlegre", 10, Posada.Luz.GRIS);
    Explorador sancho = new Explorador("Sancho", 50, solana);
    Mago gandalf = new Hechicero("Gandlf", 50, solana, 1);
    Mago campanilla = new Hada("Campanilla", 50, solana, 1);

    if(!solana.addCamino(new Camino(solana, romeral, 5))){
      System.out.println("Error en addCamino1");
    }
    if(!solana.addCamino(new Camino(solana, tomelloso, 10))){
      System.out.println("Error en addCamino2");
    }
    if(!ponipisador.addCamino(new Camino(ponipisador, elfoalegre, 10))){
      System.out.println("Error en addCamino3");
    }
    if(!tomelloso.addCamino(new Trampa(tomelloso, ponipisador, 10, (float)0.5, (float)0.2))){
      System.out.println("Error en addCamino4");
    }
    if(!tomelloso.addCamino(new Camino(tomelloso, elfoalegre, 5))){
      System.out.println("Error en addCamino5");
    }
    if(!romeral.addCamino(new Camino(romeral, elfoalegre, 5))){
      System.out.println("Error en addCamino6");
    }
    if(!romeral.addCamino(new Trampa(romeral, rohan, 10, (float)0.5, (float)1.0))){
      System.out.println("Error en addCamino7");
    }
    if(gandalf.recorre(romeral)){
      System.out.println("Error: el mago pudo desplazarse a una posada fuera de su nivel");
    }
    if(!gandalf.recorre(tomelloso)){
      System.out.println("Error: el mago no pudo desplazarse a una posada a su alcance");
    }
    if(gandalf.recorre(ponipisador)){
      System.out.println("Error: el mago puede recorrer trampas");
    }
    if(!gandalf.recorre(elfoalegre)){
      System.out.println("Error: el mago no puede recorrer posadas de su nivel de poder");
    }
    if(!campanilla.recorre(romeral)){
      System.out.println("Error: el hada no pudo desplazarse a una posada de luz blanca");
    }
    if(campanilla.recorre(elfoalegre)){
      System.out.println("Error: el hada pudo desplazarse a una posada demasiado oscura");
    }
    if(campanilla.recorre(rohan)){
      System.out.println("Error: el hada pudo recorrer una trampa");
    }
    if(!sancho.recorre(romeral)){
      System.out.println("Error: fallo en las funcionalidades básicas");
    }
    if(sancho.recorre(rohan)){
      System.out.println("Error: fallo en la trampa");
    }
    System.out.println(sancho);
    System.out.println(gandalf);
    System.out.println(campanilla);
    System.out.println(Camino.getTotal());
  }
}
