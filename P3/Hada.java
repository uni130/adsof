/**
*Esta clase define el objeto Hada
*@author Miguel Díaz y Miguel Ángel Luque
*/
import java.util.*;
public class Hada extends Mago{
  /**
  *@param n el nombre del explorador
  *@param e la energia que tiene el explorador
  *@param u la posada en la que se encuentra el explorador al empezar
  *@param p poder del mago
  */
  public Hada(String n, int e, Posada u, int p){
    super(n,e,u,p);
  }

  /**
  *@return boolean true si el hada se puede alojar en la posada p
  *@param Posada p la posada en la que se quiere comprobar si el hada se puede alojar
  */
  public boolean puedeAlojarseEn(Posada p){
    if(p.getLuz().compareTo(Posada.Luz.GRIS)>0 && !p.tieneHechicero()){
      return true;
    }else{
      return false;
    }
  }
}
