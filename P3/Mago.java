/**
*Este fichero define la clase abstracta Mago
*@author Miguel Díaz y Miguel Ángel Luque
*/
import java.util.*;
public abstract class Mago extends Explorador{

  protected int poder;

  /**
  *@param n el nombre del explorador
  *@param e la energia que tiene el explorador
  *@param u la posada en la que se encuentra el explorador al empezar
  *@param p poder del mago
  */
  public Mago(String n, int e, Posada u, int p){
    super(n,e,u);
    poder = p;
  }
  
  /**
  *@return boolean true si el mago se puede alojar en la posada p
  *@param Posada p la posada en la que se quiere comprobar si el mago se puede alojar
  */
  public abstract boolean puedeAlojarseEn(Posada p);

  /**
  *@return boolean true si el explorador puede recorrer el camino c
  *@param Camino c el camino para comprobar si el explorador lo puede recorrer
  */
  public boolean puedeRecorrerCamino(Camino c){
    if(c.costeReal() > energia || ubicacion != c.getOrigen()||(c.esTrampa())){
      return false;
    }else{
      return true;
    }
  }
}
