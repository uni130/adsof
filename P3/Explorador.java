/**
*Esta clase define el bjeto Explorador
*@author Miguel Díaz y Miguel Ángel Luque
*/

import java.util.*;
public class Explorador{
  private String nombre;
  protected int energia;
  protected Posada ubicacion;
  /**
  *@param String n el nombre del explorador
  *@param int la energia que tiene el explorador
  *@param Posada u la posada en la que se encuentra el explorador al empezar
  */
  public Explorador(String n, int e, Posada u){
    nombre = n;
    energia = e;
    ubicacion = u;
  }

  /**
  *@return String el nombre del explorador
  */
  public String getNombre(){
    return nombre;
  }

  /**
  *@return int la energia que tiene el explorador
  */
  public int getEnergia(){
    return energia;
  }

  /**
  *@return Posada la posada en la que se encuetra el explorador
  */
  public Posada getUbicacion(){
    return ubicacion;
  }

  /**
  *@return string la cadena para imprimir el explorador
  */
  public String toString(){
    return nombre + " (e:" + energia + ") en " + ubicacion.getNombre();
  }

  /**
  *@return boolean true si el explorador se puede alojar en la posada p
  *@param Posada p la posada en la que se quiere comprobar si el explorador se puede alojar
  */
  public boolean puedeAlojarseEn(Posada p){
    return true;
  }

  /**
  *@return boolean true si el explorador puede recorrer el camino c
  *@param Camino c el camino para comprobar si el explorador lo puede recorrer
  */
  public boolean puedeRecorrerCamino(Camino c){
    if(c.costeReal() > energia || ubicacion != c.getOrigen()){
      return false;
    }else{
      return true;
    }
  }

  /**
  *Hace que el explorador recorra un camino
  *@return boolean true si el explorador ha recorrido el camino y false si no ha podido recorrerlo
  *@param Camino c el camino que tiene que recorrer
  */
  public boolean recorre(Camino c){
    Posada dest;
    if(c == null){
      return false;
    }

    if(puedeRecorrerCamino(c) && puedeAlojarseEn(c.getDestino())){
      dest = c.getDestinoReal();
      energia -= c.costeReal();
      ubicacion = dest;
      energia += dest.getRecarga();
      if(dest.equals(c.getOrigen())){
        return false;
      }
    }
    return true;
  }

/**
*@return boolean true si el explorador ha podido pasar por orden por todas estas posadas
*@param posadas conjunto de posadas que intentara recorrer por orden el explorador
*/

  public boolean recorre(Posada...posadas){
    int i;
    boolean status = true;

    if(posadas.length == 1){
      return recorre(ubicacion.getCamino(posadas[0]));
    }

    for(i = 0;i < posadas.length;i++){
      if(!recorre(ubicacion.getCamino(posadas[i]))){
        status = false;
      }
    }
    return status;
  }



}
