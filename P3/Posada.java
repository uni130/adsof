import java.util.*;
/**
*
*Esta clase contiene los metodos y atributos relacionados a Posada
*ademas de la enumeracion Luz que da los posibles tipos de luz de la misma
*@author Miguel Angel Luque y Miguel Diaz
*
*/

public class Posada{
  public enum Luz
  {
    DIABOLICA, NEGRA, TENEBROSA, GRIS, CLARA, BLANCA, DIVINA
  }
  private String nombre;
  private int recarga;
  private ArrayList<Camino> caminos = new ArrayList<Camino>();
  private ArrayList<Explorador> exploradores = new ArrayList<Explorador>();
  private Luz luz = Luz.BLANCA;

  /**
  *@param n string con el nombre de la posada
  */
  public Posada(String n){
    nombre = n;
    recarga = 2;
  }

  /**
  *@param n string con el nombre de la posada
  *@param rec recarga de energia ue recibe cada exlorador en la posada
  */
  public Posada(String n,int rec){
    nombre = n;
    recarga = rec;
  }

  /**
  *@param n string con el nombre de la posada
  *@param rec recarga de energia ue recibe cada exlorador en la posada
  *@param l luz que entra en la posada
  */
  public Posada(String n,int rec,Luz l){
    nombre = n;
    recarga = rec;
    luz = l;
  }

  /**
  *@param c camino a añadir en la lista de caminos que parten de esta posada
  *@return true en caso de que el camino parta de esta posada y no vaya a si misma y false en caso contrario
  */
  public boolean addCamino(Camino c){
    if(c == null){
      return false;
    }
    if(this.nombre.equals(c.getOrigen().nombre) && !this.nombre.equals(c.getDestino().nombre)){
      caminos.add(c);
      return true;
    }else{
      return false;
    }
  }

  /**
  *@param l luz que entra en la posada a partir de ahora
  */
  public void cambiarLuz(Luz luz){
    this.luz = luz;
  }

  public Luz getLuz(){
    return this.luz;
  }

  /**
  *
  *@return nombre de la posada
  */
  public String getNombre(){
    return nombre;
  }

  /**
  *
  *@return valor que representa la recarga que hace el explorador al descansar en la posada
  */
  public int getRecarga(){
    return recarga;
  }

  /**
  *Comprueba si hay un hada alojada en la posada
  *@return true si hay un hada, false si no hay ninguna
  */
  public boolean tieneHada(){
    for(Explorador e: exploradores){
      if(e instanceof Hada){
        return true;
      }
    }
    return false;
  }

  /**
  *Comprueba si hay un hechicero alojado en la posada
  *@return true si hay un hechicero, false si no hay ninguno
  */
  public boolean tieneHechicero(){
    for(Explorador e: exploradores){
      if(e instanceof Hechicero){
        return true;
      }
    }
    return false;
  }

  /**
  *elimina un explorador a la lista de exploradores que están en la posada
  *@param e el explorador que se quiere eliminar
  */
  public void removeExplorador(Explorador e){
    exploradores.remove(e);
  }

  /**
  *Añade un explorador a la lista de exploradores que están en la posada
  *@param e el explorador que se quiere añadir
  */
  public void addExplorador(Explorador e){
    exploradores.add(e);
  }

  /**
  *Añade un hada a la lista de exploradores que están en la posada
  *@param e el hada que se quiere añadir
  */
  public void addExplorador(Hada e){
    luz = Luz.values()[luz.ordinal() + 1];
    exploradores.add(e);
  }

  /**
  *Añade un hechicero a la lista de exploradores que están en la posada
  *@param e el hechicero que se quiere añadir
  */
  public void addExplorador(Hechicero e){
    luz = Luz.values()[luz.ordinal() - 1];
    exploradores.add(e);
  }

  /**
  *@param num es la posición del array de caminos en la que se buscará
  *@return camino en la posicion del parametro num del array(empezando desde el 0)
  */
  public Camino getCamino(int num){
    if(this.getNumCaminos() <= num){
      return null;
    }
    return caminos.get(num);
  }

  /**
  *
  *@return numero de caminos existentes con origen en la posada
  */
  public int getNumCaminos(){
    return caminos.size();
  }

  /**
  *@param p posada a la cual se le busca una ruta directa desde la actual
  *@return camino buscado si existe y en caso contrario null
  */
  public Camino getCamino(Posada p){
    int i;
    Camino camino;
    for(i = 0;i < this.getNumCaminos();i++){
      camino = this.getCamino(i);
      if(p.nombre.equals(camino.getDestino().nombre)){
        return camino;
      }
    }

    return null;
  }

  /**
  *
  *@return cadena para dar los datos de la posada y los caminos que salen de ella
  */
  public String toString(){
    int i;
    String retorno;
    Camino camino;
    retorno = nombre + "(" + recarga + ")";
    if(this.getNumCaminos() > 0){
      retorno = retorno + "[";
      for(i = 0;i < this.getNumCaminos();i++){
        if(i != 0){
          retorno = retorno + ", ";
        }
        camino = this.getCamino(i);
        retorno = retorno + camino;
      }
      retorno = retorno + "]";
    }
    return retorno;
  }
}
