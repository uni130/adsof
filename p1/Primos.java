import java.util.*;
/**
* Esta clase contiene lo necesario para verificar si un número es primo
*
* @author Miguel Angel Luque Lopez miguela.luque@estudiante.uam.es
*
*/
public class Primos {

  // usamos un conjunto ordenado, que implementa TreeSet
  private SortedSet<Integer> primos;
  private int max;

  public Primos (){
    this.primos = new TreeSet<>();
    this.max = 1;
  }
  /**
  *
  * @return cache con los primos calculados
  */
  public SortedSet<Integer> getPrimos(){
    SortedSet<Integer> aux = new TreeSet<>();
    aux.addAll(primos);
    return aux;
  }

  /**
  * @param n entero hasta el cual actualizaremos la lista de primos
  * @return booleano que nos confirma si el entero se encuentra en la lista de primos o no
  */
  public boolean esPrimo(int n){
    if (n<2) return false;
    if (n>max) actualizaPrimos(n);
    return primos.contains(n);
  }

  /**
  * @param n entero que comprobaremos si es primo en la ejecución
  * @return booleano que nos confirma si el entero de la entrada es o no primo
  */
  private boolean compruebaPrimo(int n) {
    boolean primo = true;
    for(int p:primos) {
        if (n % p == 0) {
            primo = false;
            break;
        }
    }
    if (primo)return true;
    else return false;
}

/**
* @param n entero hasta el cual actualizaremos la lista de primos
*
*/
  private void actualizaPrimos(int n){
    int i;
    boolean check;
    for(i = (max+1);i <= n;i++){
      check = compruebaPrimo(i);
      if(check) primos.add(i);
    }
    max = n;
  }

  /**
  * @param n entero no primo del cual veremos sus divisores primos
  * @return divisores primos de la entrada
  */
  public SortedSet<Integer> divisoresPrimos(int n){
    SortedSet<Integer> divisores = new TreeSet<>();
    for(int p:primos) {
        if (n % p == 0) {
            divisores.add(p);
        }
    }
    return divisores;
  }

  /**
  *
  * @return Cadena que representa la concatenación de los primos almacenados
  */
  public String toString(){
    return "Primos hasta " + max + " = " + primos;
  }

  /**
  *
  *Punto de entrada de la aplicación
  *
  * Este método comprobará si un entero es primo o no
  * con la intención de probar el funcionamiento de la clase
  *
  * @param args entero recibido por la terminal
  */
  public static void main(String[] args){
    if (args.length!=1) {
    System.out.println("Se espera al menos dos números como parámetros");
    }else {
      Primos p = new Primos();
      int entrada = Integer.parseInt(args[0]);
      boolean n = p.esPrimo(entrada);
      if(n == true) System.out.println(entrada + " es un número primo");
      else{
        SortedSet<Integer> div = p.divisoresPrimos(entrada);
        System.out.println(entrada + " no es un número primo, sus divisores primos son: " + div);
      }
      System.out.println(p);
  }
  }
}
